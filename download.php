<?php
$imagem = $_GET['imagem'];
$nomeimagem = $_GET['evento'].'-'.date("d-His");

$tamanho  = filesize($imagem);
$extensao = substr($imagem, -3);

if($extensao == 'jpg' || $extensao == 'JPG' || $extensao == 'png' || $extensao == 'gif'){
    header("Content-Type: application/save");
    header("Content-Length: " . $tamanho);
    header("Content-Disposition: attachment; filename=" . $nomeimagem.'.'.$extensao);
    header("Content-Transfer-Encoding: binary");

    $fp = fopen($imagem, "r");
    fpassthru($fp);
    fclose($fp);
} else {
    echo "Ops! Voce nao pode fazer isso!";
    return false;
}   