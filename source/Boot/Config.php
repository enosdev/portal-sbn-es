<?php
if( strpos($_SERVER['HTTP_HOST'], "localhost") !== false ){
    ini_set('display_errors',1);
    ini_set('display_startup_erros',1);
    error_reporting(E_ALL);
}
// date_default_timezone_set("America/Sao_Paulo");
date_default_timezone_set("America/Bahia");
ini_set('memory_limit', '1024M');
/**
 * DATABASE
 */
if (strpos($_SERVER['HTTP_HOST'], "localhost") !== false) {
    define("CONF_DB_HOST", "localhost");
    define("CONF_DB_USER", "root");
    define("CONF_DB_PASS", "root");
    define("CONF_DB_NAME", "sbn_es");
} else {
    define("CONF_DB_HOST", "localhost");
    define("CONF_DB_USER", "u245422331_jao");
    define("CONF_DB_PASS", "yV9^TzJu");
    define("CONF_DB_NAME", "u245422331_db");
}

/**
 * PROJECT URLs
 */
define("CONF_URL_BASE", "https://www.portalsbn.com.br");
define("CONF_URL_TEST", "http://localhost/sbn_es");
define("CONF_URL_STATE", "https://www.portalsbn.com");

/**
 * SITE
 */
define("CONF_SITE_NAME", "Portal SBN");
define("CONF_SITE_TITLE", "Sistema Brasileiro de Notícias");
define("CONF_SITE_DESC", "Sistema Brasileiro de Notícias");
define("CONF_SITE_LANG", "pt_BR");
define("CONF_SITE_DOMAIN", "portalsbn.com.br");
define("CONF_SITE_VERSION", "1.0.13");
define("CONF_SITE_ADDR_STREET", "Av. Panhossi – Jardim Liberdade");
define("CONF_SITE_ADDR_NUMBER", "111");
define("CONF_SITE_ADDR_COMPLEMENT", "Bloco A, Sala 208");
define("CONF_SITE_ADDR_CITY", "Teixeira de Freitas");
define("CONF_SITE_ADDR_STATE", "BA");
define("CONF_SITE_ADDR_ZIPCODE", "45995-000");
define("CONF_SITE_ADDR_TELL", "73 3013.1414 | 9994.1049");
define("CONF_SITE_ADDR_EMAIL", "portalsbn@hotmail.com");

/**
 * SOCIAL
 */
define("CONF_SOCIAL_TWITTER_CREATOR", "PortalSBN");
define("CONF_SOCIAL_TWITTER_PUBLISHER", "PortalSBN");
define("CONF_SOCIAL_FACEBOOK_APP", ""); //1708726265823765
define("CONF_SOCIAL_FACEBOOK_PAGE", "PortalSBN");
define("CONF_SOCIAL_FACEBOOK_AUTHOR", "PortalSBN");
define("CONF_SOCIAL_GOOGLE_PAGE", "5555555555");
define("CONF_SOCIAL_GOOGLE_AUTHOR", "5555555555");
define("CONF_SOCIAL_INSTAGRAM_PAGE", "portalsbn");
define("CONF_SOCIAL_YOUTUBE_PAGE", "channel/UCOj-_pDHgluxhRVjtFqC6YA");
define("CONF_SOCIAL_WHATSAPP", "#");
define("CONF_SOCIAL_TELEGRAM", "#");

/**
 * DATES
 */
define("CONF_DATE_BR", "d/m/Y H:i:s");
define("CONF_DATE_APP", "Y-m-d H:i:s");

/**
 * PASSWORD
 */
define("CONF_PASSWD_MIN_LEN", 8);
define("CONF_PASSWD_MAX_LEN", 40);
define("CONF_PASSWD_ALGO", PASSWORD_DEFAULT);
define("CONF_PASSWD_OPTION", ["cost" => 10]);

/**
 * VIEW
 */
define("CONF_VIEW_PATH", __DIR__ . "/../../shared/views");
define("CONF_VIEW_EXT", "php");
define("CONF_VIEW_THEME", "portalsbn");
define("CONF_VIEW_APP", "cafeapp");
define("CONF_VIEW_ADMIN", "esdadm");

/**
 * UPLOAD
 */
define("CONF_UPLOAD_DIR", "storage");
define("CONF_UPLOAD_IMAGE_DIR", "images");
define("CONF_UPLOAD_FILE_DIR", "files");
define("CONF_UPLOAD_MEDIA_DIR", "medias");
define("CONF_UPLOAD_GALLERY_DIR", "gallery");

/**
 * PATH ADMIN
 */
define("PATH_ADMIN","adminsbn");

/**
 * IMAGES
 */
define("CONF_IMAGE_CACHE", CONF_UPLOAD_DIR . "/" . CONF_UPLOAD_IMAGE_DIR . "/cache");
define("CONF_IMAGE_SIZE", 2000);
define("CONF_IMAGE_QUALITY", ["jpg" => 75, "png" => 5]);

/**
 * MAIL
 */
define("CONF_MAIL_HOST", "smtp.sendgrid.net");
define("CONF_MAIL_PORT", "587");
define("CONF_MAIL_USER", "apikey");
define("CONF_MAIL_PASS", "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");
define("CONF_MAIL_SENDER", ["name" => "Robson V. Leite", "address" => "sender@email.com"]);
define("CONF_MAIL_SUPPORT", "sender@support.com");
define("CONF_MAIL_OPTION_LANG", "br");
define("CONF_MAIL_OPTION_HTML", true);
define("CONF_MAIL_OPTION_AUTH", true);
define("CONF_MAIL_OPTION_SECURE", "tls");
define("CONF_MAIL_OPTION_CHARSET", "utf-8");

/**
 * PAGAR.ME
 */
define("CONF_PAGARME_MODE", "test");
define("CONF_PAGARME_LIVE", "ak_live_*****");
define("CONF_PAGARME_TEST", "ak_test_*****");
define("CONF_PAGARME_BACK", CONF_URL_BASE . "/pay/callback");