<?php

/**
 * ####################
 * ###   VALIDATE   ###
 * ####################
 */

/**
 * @param string $email
 * @return bool
 */
function is_email(string $email): bool
{
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

/**
 * @param string $password
 * @return bool
 */
function is_passwd(string $password): bool
{
    if (password_get_info($password)['algo'] || (mb_strlen($password) >= CONF_PASSWD_MIN_LEN && mb_strlen($password) <= CONF_PASSWD_MAX_LEN)) {
        return true;
    }

    return false;
}

/**
 * ##################
 * ###   STRING   ###
 * ##################
 */

/**
 * @param string $string
 * @return string
 */
function str_slug(string $string): string
{
    $string = filter_var(mb_strtolower($string), FILTER_SANITIZE_STRIPPED);
    $formats = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()_-+={[}]/?;:.,\\\'<>°ºª';
    $replace = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                                 ';

    $slug = str_replace(["-----", "----", "---", "--"], "-",
        str_replace(" ", "-",
            trim(strtr(utf8_decode($string), utf8_decode($formats), $replace))
        )
    );
    return $slug;
}

/**
 * @param string $string
 * @return string
 */
function str_studly_case(string $string): string
{
    $string = str_slug($string);
    $studlyCase = str_replace(" ", "",
        mb_convert_case(str_replace("-", " ", $string), MB_CASE_TITLE)
    );

    return $studlyCase;
}

/**
 * @param string $string
 * @return string
 */
function str_camel_case(string $string): string
{
    return lcfirst(str_studly_case($string));
}

/**
 * @param string $string
 * @return string
 */
function str_title(string $string): string
{
    return mb_convert_case(filter_var($string, FILTER_SANITIZE_SPECIAL_CHARS), MB_CASE_TITLE);
}

/**
 * @param string $text
 * @return string
 */
function str_textarea(string $text): string
{
    $text = filter_var($text, FILTER_SANITIZE_STRIPPED);
    $arrayReplace = ["&#10;", "&#10;&#10;", "&#10;&#10;&#10;", "&#10;&#10;&#10;&#10;", "&#10;&#10;&#10;&#10;&#10;"];
    return "<p>" . str_replace($arrayReplace, "</p><p>", $text) . "</p>";
}

/**
 * @param string $string
 * @param int $limit
 * @param string $pointer
 * @return string
 */
function str_limit_words(string $string, int $limit, string $pointer = "..."): string
{
    $string = trim(filter_var($string, FILTER_SANITIZE_SPECIAL_CHARS));
    $arrWords = explode(" ", $string);
    $numWords = count($arrWords);

    if ($numWords < $limit) {
        return htmlspecialchars_decode($string);
    }

    $words = implode(" ", array_slice($arrWords, 0, $limit));
    // return "{$words}{$pointer}";
    return htmlspecialchars_decode($words).$pointer;
}

/**
 * @param string $string
 * @param int $limit
 * @param string $pointer
 * @return string
 */
function str_limit_chars(string $string, int $limit, string $pointer = "..."): string
{
    $string = trim(filter_var($string, FILTER_SANITIZE_SPECIAL_CHARS));
    if (mb_strlen($string) <= $limit) {
        return htmlspecialchars_decode($string);
    }

    $chars = mb_substr($string, 0, mb_strrpos(mb_substr($string, 0, $limit), " "));
    // return "{$chars}{$pointer}";
    return htmlspecialchars_decode($chars).$pointer;
}

/**
 * @param string $price
 * @return string
 */
function str_price(?string $price): string
{
    return number_format((!empty($price) ? $price : 0), 2, ",", ".");
}

/**
 * @param string|null $search
 * @return string
 */
function str_search(?string $search): string
{
    if (!$search) {
        return "all";
    }

    $search = preg_replace("/[^a-z0-9A-Z\@\ ]/", "", $search);
    return (!empty($search) ? $search : "all");
}

/**
 * ###############
 * ###   URL   ###
 * ###############
 */

/**
 * @param string $path
 * @return string
 */
function url(string $path = null): string
{
    if (strpos($_SERVER['HTTP_HOST'], "localhost") !== false) {
        if ($path) {
            return CONF_URL_TEST . "/" . ($path[0] == "/" ? mb_substr($path, 1) : $path);
        }
        return CONF_URL_TEST;
    }

    if ($path) {
        return CONF_URL_BASE . "/" . ($path[0] == "/" ? mb_substr($path, 1) : $path);
    }

    return CONF_URL_BASE;
}

/**
 * @return string
 */
function url_back(): string
{
    return ($_SERVER['HTTP_REFERER'] ?? url());
}

/**
 * @param string $url
 */
function redirect(string $url): void
{
    header("HTTP/1.1 302 Redirect");
    if (filter_var($url, FILTER_VALIDATE_URL)) {
        header("Location: {$url}");
        exit;
    }

    if (filter_input(INPUT_GET, "route", FILTER_DEFAULT) != $url) {
        $location = url($url);
        header("Location: {$location}");
        exit;
    }
}

/**
 * ##################
 * ###   ASSETS   ###
 * ##################
 */

/**
 * @return \Source\Models\User|null
 */
function user(): ?\Source\Models\User
{
    return \Source\Models\Auth::user();
}

/**
 * @return \Source\Core\Session
 */
function session(): \Source\Core\Session
{
    return new \Source\Core\Session();
}

/**
 * @param string|null $path
 * @param string $theme
 * @return string
 */
function theme(string $path = null, string $theme = CONF_VIEW_THEME): string
{
    if (strpos($_SERVER['HTTP_HOST'], "localhost") !== false) {
        if ($path) {
            return CONF_URL_TEST . "/themes/{$theme}/" . ($path[0] == "/" ? mb_substr($path, 1) : $path);
        }

        return CONF_URL_TEST . "/themes/{$theme}";
    }

    if ($path) {
        return CONF_URL_BASE . "/themes/{$theme}/" . ($path[0] == "/" ? mb_substr($path, 1) : $path);
    }

    return CONF_URL_BASE . "/themes/{$theme}";
}

/**
 * @param string $image
 * @param int $width
 * @param int|null $height
 * @return string
 */
function image(?string $image, int $width, int $height = null): ?string
{
    if ($image) {

        $file = url().'/'.CONF_UPLOAD_DIR.'/'.$image;
        $img = pathinfo($file);
        if($img['extension'] == 'gif'){
            return $file;
        } else {
            return url() . "/" . (new \Source\Support\Thumb())->make($image, $width, $height)."?v=".time();
        }

        //return url() . "/" . (new \Source\Support\Thumb())->make($image, $width, $height);
    }

    return null;
}

function dataPublicidade($dataFuturo): string
{                   
    // $dataAtual = date("Y-m-d H:i:s")
    // $date_time  = new DateTime($dataAtual);
    // $diff       = $date_time->diff( new DateTime($dataFuturo));
    // print_r($diff);
    // return $diff->format('%y ano(s), %m mês(s), %d dia(s), %H hora(s), %i minuto(s) e %s segundo(s)');

    #Informamos as datas e horários de início e fim no formato Y-m-d H:i:s e os convertemos para o formato timestamp
    $dia_hora_atual = strtotime(date("Y-m-d H:i:s"));
    $dia_hora_evento = strtotime($dataFuturo);
    #Achamos a diferença entre as datas...
    $diferenca = $dia_hora_evento - $dia_hora_atual;
    #Fazemos a contagem...
    $dias = intval($diferenca / 86400);
    $marcador = $diferenca % 86400;
    $hora = intval($marcador / 3600);
    $marcador = $marcador % 3600;
    $minuto = intval($marcador / 60);
    $segundos = $marcador % 60;
    #Exibimos o resultado
    
    $dias = ($dias == 0) ? '' : (($dias <= 1) ? "{$dias} dia," : "{$dias} dias,");
    $hora = ($hora == 0) ? '' : "{$hora}h";
    $minuto = ($minuto == 0) ? '' : "{$minuto}min";
    $segundos = ($segundos == 0) ? '' : "{$segundos}s";

    return "{$dias} {$hora} {$minuto} {$segundos}";
}


function getBanner($w,$h,$pg,$lc): string
{
    $page600 = $pg.$w.'x600';
    $local600 = $page600.'_'.$lc;

    $date = date("Y-m-d H:i:s");
    $read = (new \Source\Models\Publicity())->findPublicity("local LIKE '%{$local600}%' && page LIKE '%{$page600}%' && publicity_at < NOW() && publicity_out > NOW()")->order('RAND()')->fetch();
    if($read):
        if($read->link == '#'){
            return '<img class="img-fluid" style=" width:' . $w . 'px; max-height:600px;" src="' . url() . '/' . CONF_UPLOAD_DIR . '/' . $read->photo . '" />';
        } else {
            $link = str_replace(['https://','http://'], '', $read->link);
            return '<a href="//'.$link.'" target="_blank"><img class="img-fluid" style=" width:' . $w . 'px; max-height:600px;" src="' . url() . '/' . CONF_UPLOAD_DIR . '/' . $read->photo . '" alt="'.$read->name.'" title="'.$read->name.'" /></a>';
        }
    else:

        if($lc == 99){
            $page = $pg.'popup';
            $local = $page.'_1';
        } else {
            $page = $pg.$w.'x'.$h;
            $local = $page.'_'.$lc;
        }

        $read = (new \Source\Models\Publicity())->findPublicity("local LIKE '%{$local}%' && page LIKE '%{$page}%' && publicity_at < NOW() && publicity_out > NOW()")->order('RAND()')->fetch();
        if($read):

            if($lc == 99){

                if($read->link == '#'){
                    $returnPop = '<img src="'.url("/".CONF_UPLOAD_DIR."/{$read->photo}").'" alt="'.$read->name.'" title="'.$read->name.'"/>';
                } else {
                    $link = str_replace(['https://','http://'], '', $read->link);
                    $returnPop = '<a href="//'.$link.'" target="_blank"><img src="'.url("/".CONF_UPLOAD_DIR."/{$read->photo}").'" alt="'.$read->name.'" title="'.$read->name.'"/></a>';
                }

                $return = '
                    <div class="modal-container md-popup">
                    <div class="modal">
                        <div class="modal-bg"></div>
                        <div class="modal-box">
                            <div class="modal-box-action"><i class="fa fa-close"></i></div>
                            <div class="modal-box-content">
                                '.$returnPop.'
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <script>$(document).ready(function () {getModal("md-popup");});</script>
                ';
                return $return;
            }

            if($read->link == '#'){
                return '<img class="img-fluid" style=" width:' . $w . 'px; max-height:' . $h . 'px;" src="' . url() . '/' . CONF_UPLOAD_DIR . '/' . $read->photo . '" />';
            } else {
                $link = str_replace(['https://','http://'], '', $read->link);
                return '<a href="http://'.$link.'" target="_blank"><img class="img-fluid" style=" width:' . $w . 'px; max-height:' . $h . 'px;" src="' . url() . '/' . CONF_UPLOAD_DIR . '/' . $read->photo . '" alt="'.$read->name.'" title="'.$read->name.'" /></a>';
            }
        else:
            if($lc == 99){
                return false;
            }
            return '<img class="img-fluid" src="'.theme('/img/banner_'.$w.'_'.$h.'.png').'" alt="">';
        endif;
    endif;
}

/**
 * @param int $local
 * @param int $width
 * @param int $height
 * @return string|null
 */
function bannerAds(int $local, int $width = null, int $height = null)
{
    $page = 'inicial';
    $url_request = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $nova_url = str_replace(['http://', 'https://'], '', url());

    $nova_url = str_replace($nova_url, '', $url_request);

    //ver o banner nas subcategorias
    if (strpos($nova_url, 'artigo/em/') !== false) {
        $url = explode('/',$_SERVER['REQUEST_URI']);
        $pag = (new \Source\Models\Category())->findByUri(end($url));
        if($pag->parent == NULL){
            $page = $pag->uri;
        } else {
            $page = (new \Source\Models\Category())->findById($pag->parent)->uri;
        }
        return getBanner($width,$height,$page,$local);
    }

    //ver o banner na categoria principal
    if (strpos($nova_url, 'artigo/lista/') !== false) {
        $url = explode('/',$_SERVER['REQUEST_URI']);
        $page = (new \Source\Models\Category())->findByUri(end($url))->uri;
        return getBanner($width,$height,$page,$local);
    }

    //ver o banner dentro das matérias
    if (strpos($nova_url, 'artigo') !== false) {
        $url = explode('/',$_SERVER['REQUEST_URI']);
        $readNews = (new \Source\Models\Post())->findByUri(end($url));
        if($readNews):
            $catUri = $readNews->category()->parent;
            if($catUri == NULL){
                $page = $readNews->category()->uri;
            } else {
                $page = (new \Source\Models\Category())->findById($catUri)->uri;
            }
            //returna o banner
            return getBanner($width,$height,$page,$local);
        else:
            //returna o banner
            return getBanner($width,$height,'entretenimento',$local);
        endif;
    }

    //ver o banner no entretenimento
    if (strpos($nova_url, 'entretenimento') !== false) {
        $url = explode('/',$_SERVER['REQUEST_URI']);
        $page = (new \Source\Models\Category())->findByUri(end($url))->uri;
        return getBanner($width,$height,$page,$local);
    }

   
    //página inicial
   return getBanner($width,$height,$page,$local);

//    return "<div class=\"modal-container md-popup\"><div class=\"modal\"><div class=\"modal-bg\"></div><div class=\"modal-box\"><div class=\"modal-box-action\"><i class=\"fa fa-close\"></i></div><div class=\"modal-box-content\">"
//                         . "<img src=\"" . HOME . "/uploads/publicidade/{$Cover}\"/>"
//                         . "</div><div class=\"clear\"></div></div></div></div>"
//                         . "<script>$(document).ready(function () {getModal('md-popup');});</script>";

    //$read = (new \Source\Models\Publicity())->findPublicity('local = :l && page LIKE \'%'.$page.'%\'',"l={$local}")->order('RAND()')->fetch();
    // $read = (new \Source\Models\Publicity())->findPublicity('local = :l && page LIKE \'%' . $page . '%\'', "l={$local}")->order('RAND()')->limit(5)->fetch(true);

    // if ($read) {

    //     foreach ($read as $rea) {
    //         if ($rea->local == 1) {
    //             echo '
    //         <script>
    //             $(document).ready(function() {
    //                 $(".fancybox").fancybox();
    //                 $.fancybox("' . url() . '/' . CONF_UPLOAD_DIR . '/' . $rea->photo . '");
    //             });
    //         </script>
    //         ';
    //         } else {
    //             if ($rea->link == '#'):
    //                 echo '<div style="display: none"><img class="img-fluid" style=" width:' . $width . 'px; max-height:' . $height . 'px;" src="' . url() . '/' . CONF_UPLOAD_DIR . '/' . $rea->photo . '" /></div>';
    //             else:
    //                 //$link = str_replace(['https://', 'http://'], '', $rea->link);
    //                 $link = str_replace(['https://'], 'http://', $rea->link);
    //                 echo '<div style="display: none"><a href="' . $link . '" target="_blank"><img class="img-fluid" style="width:' . $width . 'px; max-height:' . $height . 'px;" src="' . url() . '/' . CONF_UPLOAD_DIR . '/' . $rea->photo . '" /></a></div>';
    //             endif;
    //         }
    //     }

    // } else {
    //     if ($local == 1) {
    //         return;
    //     } else {
    //         echo '<img class="img-fluid" src="' . theme('/assets/img/' . $width . 'x' . $height . '.jpg') . '" alt="banner">';
    //     }
    // }

}


/**
 * mostra mes para os artivos
 */
function month($data): string
{
    $pega = date('m', strtotime($data));
    switch ($pega):
case '01':
    $m = 'JAN';
    break;
case '02':
    $m = 'FEV';
    break;
case '03':
    $m = 'MAR';
    break;
case '04':
    $m = 'ABR';
    break;
case '05':
    $m = 'MAI';
    break;
case '06':
    $m = 'JUN';
    break;
case '07':
    $m = 'JUL';
    break;
case '08':
    $m = 'AGO';
    break;
case '09':
    $m = 'SET';
    break;
case '10':
    $m = 'OUT';
    break;
case '11':
    $m = 'NOV';
    break;
case '12':
    $m = 'DEZ';
    break;
    endswitch;

    return $m;
}

/**
 * data do topo do site
 */
function diaMesAno(): string
{
        $dias = ['Domingo', 'Segunda-Feira', 'Terça-Feira', 'Quarta-Feira', 'Quinta-Feira', 'Sexta-Feira', 'Sabado'];
        $mes = ['', 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

        $dia_semana = date('w', time());
        $dia = date('d');
        $ano = date('Y');

        return "<b>{$dias[$dia_semana]}</b>, {$dia} de {$mes[date('m')]} de {$ano}, " . date('H:i');
}

/**
 * ################
 * ###   DATE   ###
 * ################
 */

/**
 * @param string $date
 * @param string $format
 * @return string
 * @throws Exception
 */
function date_fmt(?string $date, string $format = "d/m/Y H\hi"): string
{
    $date = (empty($date) ? "now" : $date);
    return (new DateTime($date))->format($format);
}

/**
 * @param string $date
 * @return string
 * @throws Exception
 */
function date_fmt_br(?string $date): string
{
    $date = (empty($date) ? "now" : $date);
    return (new DateTime($date))->format(CONF_DATE_BR);
}

/**
 * @param string $date
 * @return string
 * @throws Exception
 */
function date_fmt_app(?string $date): string
{
    $date = (empty($date) ? "now" : $date);
    return (new DateTime($date))->format(CONF_DATE_APP);
}

/**
 * @param string|null $date
 * @return string|null
 */
function date_fmt_back(?string $date): ?string
{
    if (!$date) {
        return null;
    }

    if (strpos($date, " ")) {
        $date = explode(" ", $date);
        return implode("-", array_reverse(explode("/", $date[0]))) . " " . $date[1];
    }

    return implode("-", array_reverse(explode("/", $date)));
}

/**
 * ####################
 * ###   PASSWORD   ###
 * ####################
 */

/**
 * @param string $password
 * @return string
 */
function passwd(string $password): string
{
    if (!empty(password_get_info($password)['algo'])) {
        return $password;
    }

    return password_hash($password, CONF_PASSWD_ALGO, CONF_PASSWD_OPTION);
}

/**
 * @param string $password
 * @param string $hash
 * @return bool
 */
function passwd_verify(string $password, string $hash): bool
{
    return password_verify($password, $hash);
}

/**
 * @param string $hash
 * @return bool
 */
function passwd_rehash(string $hash): bool
{
    return password_needs_rehash($hash, CONF_PASSWD_ALGO, CONF_PASSWD_OPTION);
}

/**
 * ###################
 * ###   REQUEST   ###
 * ###################
 */

/**
 * @return string
 */
function csrf_input(): string
{
    $session = new \Source\Core\Session();
    $session->csrf();
    return "<input type='hidden' name='csrf' value='" . ($session->csrf_token ?? "") . "'/>";
}

/**
 * @param $request
 * @return bool
 */
function csrf_verify($request): bool
{
    $session = new \Source\Core\Session();
    if (empty($session->csrf_token) || empty($request['csrf']) || $request['csrf'] != $session->csrf_token) {
        return false;
    }
    return true;
}

/**
 * @return null|string
 */
function flash(): ?string
{
    $session = new \Source\Core\Session();
    if ($flash = $session->flash()) {
        return $flash;
    }
    return null;
}

/**
 * @param string $key
 * @param int $limit
 * @param int $seconds
 * @return bool
 */
function request_limit(string $key, int $limit = 5, int $seconds = 60): bool
{
    $session = new \Source\Core\Session();
    if ($session->has($key) && $session->$key->time >= time() && $session->$key->requests < $limit) {
        $session->set($key, [
            "time" => time() + $seconds,
            "requests" => $session->$key->requests + 1,
        ]);
        return false;
    }

    if ($session->has($key) && $session->$key->time >= time() && $session->$key->requests >= $limit) {
        return true;
    }

    $session->set($key, [
        "time" => time() + $seconds,
        "requests" => 1,
    ]);

    return false;
}

/**
 * @param string $field
 * @param string $value
 * @return bool
 */
function request_repeat(string $field, string $value): bool
{
    $session = new \Source\Core\Session();
    if ($session->has($field) && $session->$field == $value) {
        return true;
    }

    $session->set($field, $value);
    return false;
}

/**
 * @param array $array
 * @return string
 */
function google_tags(array $array):string
{
    if (strpos($_SERVER['HTTP_HOST'], "localhost") === false) {
        $string = '';
        foreach($array as $str):
            $string .= "<script async src='https://www.googletagmanager.com/gtag/js?id={$str}'></script>
            <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', '{$str}');
            </script>";
        endforeach;
        return $string;
    }
    return false;
}

/**
 * @param string $adsense
 * @param bool $body
 * @param int $min
 * @return string
 */
function adsense(string $adsense, bool $body = false, int $min = 11):string
{
    if (strpos($_SERVER['HTTP_HOST'], "localhost") === false) {
        if(date("H") % 2 || date("H") == 4 || date("H") == 6 || date("H") == 20 ){
            $code = true;
        } else {
            $code = (date("i") <= $min)? true : false;
        }
        
        $ad_client = ($code)? 'ca-pub-3621384655720559' : $adsense;

        if(!$body){   
            $return = "<script async custom-element=\"amp-auto-ads\" src=\"https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js\"></script>
            <script data-ad-client=\"{$ad_client}\" async src=\"https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script>";
            return $return;
        } else {
            return "<amp-auto-ads type=\"adsense\" data-ad-client=\"{$ad_client}\"></amp-auto-ads>";
        }
    }
    return false;
}

/**
 * @param string $link
 * @param bool $image
 * @return string
 */
function youtube_photo(string $link, bool $image = false): string
{
    if(mb_strpos($link, 'youtu.be/') !== false){
        $exp = explode('youtu.be/', $link);
        $return = end($exp);
    } else {
        $itens = parse_url ($link);
        parse_str($itens['query'], $params);
        $return = $params['v'];
    }
    $img = ($image)? 'mqdefault' : 'maxresdefault';
    //http://i.ytimg.com/vi/G9dHCwf9H7A/maxresdefault.jpg
    //https://img.youtube.com/vi/AfiHR6SWsFM/mqdefault.jpg
    //https://img.youtube.com/vi/AfiHR6SWsFM/hqdefault.jpg
    return "//img.youtube.com/vi/{$return}/{$img}.jpg";
}

/**
 * @param string $link
 * @return string
 */
function youtube_link(string $link): string
{
    if(mb_strpos($link, 'youtu.be/') !== false){
        $exp = explode('youtu.be/', $link);
        $return = end($exp);
    } else {
        $itens = parse_url ($link);
        parse_str($itens['query'], $params);
        $return = $params['v'];
    }

    return "//www.youtube.com/embed/{$return}?autoplay=1";
}

/**
 * @param int $amount
 * @return string
 */
function captcha(int $amount = 3): string
{
    // $word = substr(str_shuffle("AaBbCcDdEeFfGgHhIiJjKkLlMmNnPpQqRrSsTtUuVvYyXxWwZz23456789"),0,($amount));
    $word = substr(str_shuffle("1234567890"),0,($amount));
    $_SESSION["word"] = base64_encode($word);

    return $word;
}