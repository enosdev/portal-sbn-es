<?php

namespace Source\App\Admin;

use Source\Models\Highlight;
use Source\Models\User;
use Source\Models\Gallery;
use Source\Support\Pager;
use Source\Support\Thumb;
use Source\Support\Upload;

/**
 * Class Blog
 * @package Source\App\Admin
 */
class Dest extends Admin
{
    /**
     * Blog constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function home(?array $data): void
    {
        //search redirect
        if (!empty($data["s"])) {
            $s = str_search($data["s"]);
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/dest/home/{$s}/1")]);
            return;
        }

        $search = null;
        $posts = (new Highlight())->find("type = :type && status != :st",'type=post&st=trash');

        if (!empty($data["search"]) && str_search($data["search"]) != "all") {
            $search = str_search($data["search"]);
            $posts = (new Highlight())->find("MATCH(title, name) AGAINST(:s) && status != :st", "s={$search}&st=trash");
            if (!$posts->count()) {
                $this->message->info("Sua pesquisa não retornou resultados")->flash();
                redirect("/".PATH_ADMIN."/dest/home");
            }
        }

        $all = ($search ?? "all");
        $pager = new Pager(url("/".PATH_ADMIN."/dest/home/{$all}/"));
        $pager->pager($posts->count(), 36, (!empty($data["page"]) ? $data["page"] : 1));

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Portal",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/dest/home", [
            "app" => "dest/home",
            "head" => $head,
            "posts" => $posts->limit($pager->limit())->offset($pager->offset())->order("post_at DESC")->fetch(true),
            "paginator" => $pager->render(),
            "search" => $search
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function highlight(?array $data): void
    {
        //create
        if (!empty($data["action"]) && $data["action"] == "create") {
            $content = $data["content"];
            $content2 = $data["content2"];
            $businessperson_content = $data["businessperson_content"];
            $businessperson_content2 = $data["businessperson_content2"];
            $wordbyword = [
                $_POST['deus'],
                $_POST['familia'],
                $_POST['estudos'],
                $_POST['trabalho'],
                $_POST['moda'],
                $_POST['vida'],
                $_POST['casamento'],
                $_POST['portal']
            ];
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $postCreate = new Highlight();
            $postCreate->author = $data["author"];
            $postCreate->title_highlight = $data["title_highlight"];
            $postCreate->title = $data["title"];
            $postCreate->uri = str_slug($postCreate->title);
            $postCreate->name = $data["name"].'~'.$data["nameColor"];
            $postCreate->datasheet = implode('!!', $data["datasheet"]).'~'.$data["datasheetColor"];
            $postCreate->title2 = $data["title2"];
            $postCreate->content = $content;
            $postCreate->title3 = $data["title3"];
            $postCreate->content2 = $content2;
            $postCreate->word_by_word = implode('!!',$wordbyword);
            $postCreate->project = implode('!!', $data["project"]).'~'.$data["projectColor"];
            $postCreate->gallery = $data["gallery"];
            $postCreate->video = $data["video"];
            $postCreate->businessperson_title = $data["businessperson_title"];
            $postCreate->businessperson_title1 = $data["businessperson_title1"];
            $postCreate->businessperson_content = $businessperson_content;
            $postCreate->businessperson_photo_subtitle = $data["businessperson_photo_subtitle"].'~'.$data["businessperson_photo_subtitleColor"];
            $postCreate->businessperson_photo2_subtitle = $data["businessperson_photo2_subtitle"];
            $postCreate->businessperson_subtitle = $data["businessperson_subtitle"];
            $postCreate->businessperson_content2 = $businessperson_content2;
            $postCreate->businessperson_author = $data["businessperson_name"] ."!!". $data["businessperson_email"];
            $postCreate->status = $data["status"];
            $postCreate->type = 'post';
            $postCreate->post_at = date_fmt_back($data["post_at"]);

            //upload cover
            if (!empty($_FILES["cover"])) {
                $files = $_FILES["cover"];
                $upload = new Upload();
                $image = $upload->image($files, "cover-".$postCreate->title);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postCreate->cover = $image;
            }

            //upload cover2
            if (!empty($_FILES["cover2"])) {
                $files2 = $_FILES["cover2"];
                $upload2 = new Upload();
                $image2 = $upload2->image($files2, "cover2-".$postCreate->title);

                if (!$image2) {
                    $json["message"] = $upload2->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postCreate->cover2 = $image2;
            }

            //upload cover3
            if (!empty($_FILES["cover3"])) {
                $files3 = $_FILES["cover3"];
                $upload3 = new Upload();
                $image3 = $upload3->image($files3, "cover3-".$postCreate->title);

                if (!$image3) {
                    $json["message"] = $upload3->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postCreate->cover3 = $image3;
            }

            //upload cover4
            if (!empty($_FILES["cover4"])) {
                $files4 = $_FILES["cover4"];
                $upload4 = new Upload();
                $image4 = $upload4->image($files4, "cover4-".$postCreate->title);

                if (!$image4) {
                    $json["message"] = $upload4->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postCreate->cover4 = $image4;
            }

            //upload cover5
            if (!empty($_FILES["cover5"])) {
                $files5 = $_FILES["cover5"];
                $upload5 = new Upload();
                $image5 = $upload5->image($files5, "cover5-".$postCreate->title);

                if (!$image5) {
                    $json["message"] = $upload5->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postCreate->cover5 = $image5;
            }

            //upload publicity
            if (!empty($_FILES["publicity"])) {
                $publicity = $_FILES["publicity"];
                $upload_p = new Upload();
                $image_p = $upload_p->image($publicity, "publicity-".$postCreate->title);

                if (!$image_p) {
                    $json["message"] = $upload_p->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postCreate->publicity = $image_p.'~'.(!empty($data['publicityLink']) ? $data['publicityLink'] : '#');
            }

            //upload publicity2
            if (!empty($_FILES["publicity2"])) {
                $publicity2 = $_FILES["publicity2"];
                $upload_p2 = new Upload();
                $image_p2 = $upload_p2->image($publicity2, "publicity2-".$postCreate->title);

                if (!$image_p2) {
                    $json["message"] = $upload_p2->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postCreate->publicity2 = $image_p2.'~'.(!empty($data['publicity2Link']) ? $data['publicity2Link'] : '#');
            }

            //upload publicity3
            if (!empty($_FILES["publicity3"])) {
                $publicity3 = $_FILES["publicity3"];
                $upload_p3 = new Upload();
                $image_p3 = $upload_p3->image($publicity3, "publicity3-".$postCreate->title);

                if (!$image_p3) {
                    $json["message"] = $upload_p3->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postCreate->publicity3 = $image_p3.'~'.(!empty($data['publicity3Link']) ? $data['publicity3Link'] : '#');
            }

            //upload publicity4
            if (!empty($_FILES["publicity4"])) {
                $publicity4 = $_FILES["publicity4"];
                $upload_p4 = new Upload();
                $image_p4 = $upload_p4->image($publicity4, "publicity4-".$postCreate->title);

                if (!$image_p4) {
                    $json["message"] = $upload_p4->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postCreate->publicity4 = $image_p4.'~'.(!empty($data['publicity4Link']) ? $data['publicity4Link'] : '#');
            }

            //upload businessperson
            if (!empty($_FILES["businessperson_photo"])) {
                $businessperson_files = $_FILES["businessperson_photo"];
                $businessperson_upload = new Upload();
                $businessperson_image = $businessperson_upload->image($businessperson_files, "businessperson-".$postCreate->businessperson_title1);

                if (!$businessperson_image) {
                    $json["message"] = $businessperson_upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postCreate->businessperson_photo = $businessperson_image;
            }

            //upload businessperson
            if (!empty($_FILES["businessperson_photo2"])) {
                $businessperson_files2 = $_FILES["businessperson_photo2"];
                $businessperson_upload2 = new Upload();
                $businessperson_image2 = $businessperson_upload2->image($businessperson_files2, "businessperson2-".$postCreate->businessperson_title1);

                if (!$businessperson_image2) {
                    $json["message"] = $businessperson_upload2->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postCreate->businessperson_photo2 = $businessperson_image2;
            }

            if (!$postCreate->save()) {
                $json["message"] = $postCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Destaque publicado com sucesso...")->flash();
            $json["redirect"] = url("/".PATH_ADMIN."/dest/highlight/{$postCreate->id}");

            echo json_encode($json);
            return;
        }

        //update
        if (!empty($data["action"]) && $data["action"] == "update") {
            $content = $data["content"];
            $content2 = $data["content2"];
            $businessperson_content = $data["businessperson_content"];
            $businessperson_content2 = $data["businessperson_content2"];
            $wordbyword = [
                $_POST['deus'],
                $_POST['familia'],
                $_POST['estudos'],
                $_POST['trabalho'],
                $_POST['moda'],
                $_POST['vida'],
                $_POST['casamento'],
                $_POST['portal']
            ];
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $postEdit = (new Highlight())->findById($data["post_id"]);

            if (!$postEdit) {
                $this->message->error("Você tentou atualizar um Destaque que não existe ou foi removido")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/dest/highlight")]);
                return;
            }

            $postEdit->author = $data["author"];
            $postEdit->title_highlight = $data["title_highlight"];
            $postEdit->title = $data["title"];
            $postEdit->uri = str_slug($postEdit->title);
            $postEdit->name = $data["name"].'~'.$data["nameColor"];
            $postEdit->datasheet = implode('!!', $data["datasheet"]).'~'.$data["datasheetColor"];
            $postEdit->title2 = $data["title2"];
            $postEdit->content = $content;
            $postEdit->title3 = $data["title3"];
            $postEdit->content2 = $content2;
            $postEdit->word_by_word = implode('!!',$wordbyword);
            $postEdit->project = implode('!!', $data["project"]).'~'.$data["projectColor"];
            $postEdit->gallery = $data["gallery"];
            $postEdit->video = $data["video"];
            $postEdit->businessperson_title = $data["businessperson_title"];
            $postEdit->businessperson_title1 = $data["businessperson_title1"];
            $postEdit->businessperson_content = $businessperson_content;
            $postEdit->businessperson_photo_subtitle = $data["businessperson_photo_subtitle"].'~'.$data["businessperson_photo_subtitleColor"];
            $postEdit->businessperson_photo2_subtitle = $data["businessperson_photo2_subtitle"];
            $postEdit->businessperson_subtitle = $data["businessperson_subtitle"];
            $postEdit->businessperson_content2 = $businessperson_content2;
            $postEdit->businessperson_author = $data["businessperson_name"] ."!!". $data["businessperson_email"];         
            $postEdit->status = $data["status"];
            $postEdit->post_at = date_fmt_back($data["post_at"]);

            //upload cover
            if (!empty($_FILES["cover"])) {
                if ($postEdit->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->cover}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->cover}");
                    (new Thumb())->flush($postEdit->cover);
                }

                $files = $_FILES["cover"];
                $upload = new Upload();
                $image = $upload->image($files, "cover-".$postEdit->title);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postEdit->cover = $image;
            }

            //upload cover 2
            if (!empty($_FILES["cover2"])) {
                if ($postEdit->cover2 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->cover2}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->cover2}");
                    (new Thumb())->flush($postEdit->cover2);
                }

                $files2 = $_FILES["cover2"];
                $upload2 = new Upload();
                $image2 = $upload2->image($files2, "cover2-".$postEdit->title);

                if (!$image2) {
                    $json["message"] = $upload2->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postEdit->cover2 = $image2;
            }

            //upload cover 3
            if (!empty($_FILES["cover3"])) {
                if ($postEdit->cover3 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->cover3}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->cover3}");
                    (new Thumb())->flush($postEdit->cover3);
                }

                $files3 = $_FILES["cover3"];
                $upload3 = new Upload();
                $image3 = $upload3->image($files3, "cover3-".$postEdit->title);

                if (!$image3) {
                    $json["message"] = $upload3->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postEdit->cover3 = $image3;
            }

            //upload cover 4
            if (!empty($_FILES["cover4"])) {
                if ($postEdit->cover4 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->cover4}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->cover4}");
                    (new Thumb())->flush($postEdit->cover4);
                }

                $files4 = $_FILES["cover4"];
                $upload4 = new Upload();
                $image4 = $upload4->image($files4, "cover4-".$postEdit->title);

                if (!$image4) {
                    $json["message"] = $upload4->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postEdit->cover4 = $image4;
            }

            //upload cover 5
            if (!empty($_FILES["cover5"])) {
                if ($postEdit->cover5 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->cover5}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->cover5}");
                    (new Thumb())->flush($postEdit->cover5);
                }

                $files5 = $_FILES["cover5"];
                $upload5 = new Upload();
                $image5 = $upload5->image($files5, "cover5-".$postEdit->title);

                if (!$image5) {
                    $json["message"] = $upload5->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postEdit->cover5 = $image5;
            }

            //upload publicity
            if (!empty($_FILES["publicity"])) {
                if ($postEdit->publicity && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->publicity}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->publicity}");
                    (new Thumb())->flush($postEdit->publicity);
                }

                $files_p = $_FILES["publicity"];
                $upload_p = new Upload();
                $image_p = $upload_p->image($files_p, "publicity-".$postEdit->title);

                if (!$image_p) {
                    $json["message"] = $upload_p->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postEdit->publicity = $image_p.'~'.(!empty($data['publicityLink']) ? $data['publicityLink'] : '#');
            } else {
                $postEdit->publicity = $data['publicityImage'].'~'.(!empty($data['publicityLink']) ? $data['publicityLink'] : '#');
            }

            //upload publicity 2
            if (!empty($_FILES["publicity2"])) {
                if ($postEdit->publicity2 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->publicity2}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->publicity2}");
                    (new Thumb())->flush($postEdit->publicity2);
                }

                $files_p2 = $_FILES["publicity2"];
                $upload_p2 = new Upload();
                $image_p2 = $upload_p2->image($files_p2, "publicity2-".$postEdit->title);

                if (!$image_p2) {
                    $json["message"] = $upload_p2->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postEdit->publicity2 = $image_p2.'~'.(!empty($data['publicity2Link']) ? $data['publicity2Link'] : '#');
            } else {
                $postEdit->publicity2 = $data['publicity2Image'].'~'.(!empty($data['publicity2Link']) ? $data['publicity2Link'] : '#');
            }

            //upload publicity 3
            if (!empty($_FILES["publicity3"])) {
                if ($postEdit->publicity3 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->publicity3}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->publicity3}");
                    (new Thumb())->flush($postEdit->publicity3);
                }

                $files_p3 = $_FILES["publicity3"];
                $upload_p3 = new Upload();
                $image_p3 = $upload_p3->image($files_p3, "publicity3-".$postEdit->title);

                if (!$image_p3) {
                    $json["message"] = $upload_p3->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postEdit->publicity3 = $image_p3.'~'.(!empty($data['publicity3Link']) ? $data['publicity3Link'] : '#');
            } else {
                $postEdit->publicity3 = $data['publicity3Image'].'~'.(!empty($data['publicity3Link']) ? $data['publicity3Link'] : '#');
            }

            //upload publicity 4
            if (!empty($_FILES["publicity4"])) {
                if ($postEdit->publicity4 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->publicity4}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->publicity4}");
                    (new Thumb())->flush($postEdit->publicity4);
                }

                $files_p4 = $_FILES["publicity4"];
                $upload_p4 = new Upload();
                $image_p4 = $upload_p4->image($files_p4, "publicity4-".$postEdit->title);

                if (!$image_p4) {
                    $json["message"] = $upload_p4->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postEdit->publicity4 = $image_p4.'~'.(!empty($data['publicity4Link']) ? $data['publicity4Link'] : '#');
            } else {
                $postEdit->publicity4 = $data['publicity4Image'].'~'.(!empty($data['publicity4Link']) ? $data['publicity4Link'] : '#');
            }

            //upload cover empresario businessperson_photo
            if (!empty($_FILES["businessperson_photo"])) {
                if ($postEdit->businessperson_photo && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->businessperson_photo}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->businessperson_photo}");
                    (new Thumb())->flush($postEdit->businessperson_photo);
                }

                $businessperson_files = $_FILES["businessperson_photo"];
                $businessperson_upload = new Upload();
                $businessperson_image = $businessperson_upload->image($businessperson_files, "businessperson-".$postEdit->businessperson_title1);

                if (!$businessperson_image) {
                    $json["message"] = $businessperson_upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postEdit->businessperson_photo = $businessperson_image;
            }

            //upload cover empresario2 businessperson_photo2
            if (!empty($_FILES["businessperson_photo2"])) {
                if ($postEdit->businessperson_photo2 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->businessperson_photo2}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->businessperson_photo2}");
                    (new Thumb())->flush($postEdit->businessperson_photo2);
                }

                $businessperson_files2 = $_FILES["businessperson_photo2"];
                $businessperson_upload2 = new Upload();
                $businessperson_image2 = $businessperson_upload2->image($businessperson_files2, "businessperson2-".$postEdit->businessperson_title1);

                if (!$businessperson_image2) {
                    $json["message"] = $businessperson_upload2->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postEdit->businessperson_photo2 = $businessperson_image2;
            }


            if (!$postEdit->save()) {
                $json["message"] = $postEdit->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Destaque atualizado com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //delete
        if (!empty($data["action"]) && $data["action"] == "delete") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $postDelete = (new Highlight())->findById($data["post_id"]);

            if (!$postDelete) {
                $this->message->error("Você tentou excluir um destaque que não existe ou já foi removido")->flash();
                echo json_encode(["reload" => true]);
                return;
            }

            if ($postDelete->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->cover}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->cover}");
                (new Thumb())->flush($postDelete->cover);
            }

            if ($postDelete->cover2 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->cover2}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->cover2}");
                (new Thumb())->flush($postDelete->cover2);
            }

            if ($postDelete->cover3 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->cover3}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->cover3}");
                (new Thumb())->flush($postDelete->cover3);
            }

            if ($postDelete->cover4 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->cover4}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->cover4}");
                (new Thumb())->flush($postDelete->cover4);
            }

            if ($postDelete->cover5 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->cover5}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->cover5}");
                (new Thumb())->flush($postDelete->cover5);
            }

            if ($postDelete->publicity && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->publicity}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->publicity}");
                (new Thumb())->flush($postDelete->publicity);
            }

            if ($postDelete->publicity2 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->publicity2}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->publicity2}");
                (new Thumb())->flush($postDelete->publicity2);
            }

            if ($postDelete->publicity3 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->publicity3}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->publicity3}");
                (new Thumb())->flush($postDelete->publicity3);
            }

            if ($postDelete->publicity4 && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->publicity4}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->publicity4}");
                (new Thumb())->flush($postDelete->publicity4);
            }
            

            $postDelete->destroy();
            $this->message->success("O destaque foi excluído com sucesso...")->flash();

            echo json_encode(["reload" => true]);
            return;
        }

        //Leixeira
        if (!empty($data["action"]) && $data["action"] == "lixeira") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $postLixeira = (new Highlight())->findById($data["post_id"]);

            if (!$postLixeira) {
                $this->message->error("Você tentou apagar um destaque que não existe ou foi removido")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/blog/home")]);
                return;
            }

            $postLixeira->status = "trash";

            if (!$postLixeira->save()) {
                $json["message"] = $postLixeira->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Destaque enviado para Lixeira")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        $postEdit = null;
        if (!empty($data["post_id"])) {
            $postId = filter_var($data["post_id"], FILTER_VALIDATE_INT);
            $postEdit = (new Highlight())->findById($postId);
        }

        $head = $this->seo->render(
            CONF_SITE_NAME . " | " . ($postEdit->title ?? "Novo Destaque"),
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/dest/highlight", [
            "app" => "dest/highlight",
            "head" => $head,
            "post" => $postEdit,
            "gallery" => (new Gallery())->findGallery("entertainment = :e", "e=jor")->order("date_at DESC")->limit(5)->fetch(true),
            "authors" => (new User())->find("level >= :level", "level=5")->fetch(true)
        ]);
    }
}