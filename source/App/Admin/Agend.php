<?php

namespace Source\App\Admin;

use Source\Models\Category;
use Source\Models\Agenda;
use Source\Models\User;
use Source\Support\Pager;
use Source\Support\Thumb;
use Source\Support\Upload;

/**
 * Class Agend
 * @package Source\App\Admin
 */
class Agend extends Admin
{
    /**
     * Agend constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function home(?array $data): void
    {
        //search redirect
        if (!empty($data["s"])) {
            $s = str_search($data["s"]);
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/agend/home/{$s}/1")]);
            return;
        }

        $search = null;
        $posts = (new Agenda())->find();

        if (!empty($data["search"]) && str_search($data["search"]) != "all") {
            $search = str_search($data["search"]);
            $posts = (new Agenda())->find("MATCH(title) AGAINST(:s)", "s={$search}");
            if (!$posts->count()) {
                $this->message->info("Sua pesquisa não retornou resultados")->flash();
                redirect("/".PATH_ADMIN."/agend/home");
            }
        }

        $all = ($search ?? "all");
        $pager = new Pager(url("/".PATH_ADMIN."/agend/home/{$all}/"));
        $pager->pager($posts->count(), 12, (!empty($data["page"]) ? $data["page"] : 1));

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Agenda",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/agend/home", [
            "app" => "agend/home",
            "head" => $head,
            "posts" => $posts->limit($pager->limit())->offset($pager->offset())->order("event_date DESC")->fetch(true),
            "paginator" => $pager->render(),
            "search" => $search
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function agenda(?array $data): void
    {
        //create
        if (!empty($data["action"]) && $data["action"] == "create") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $postCreate = new Agenda();
            $postCreate->author = $data["author"];
            $postCreate->title = $data["title"];
            $postCreate->uri = str_slug($postCreate->title);
            $postCreate->event_date = date_fmt_back($data["event_date"]);
            $postCreate->city = $data["city"];
            $postCreate->local = $data["local"];
            $postCreate->details = $data["details"];
            // $postCreate->estate = $data["estate"];
            $postCreate->estate = 'ba';
            $postCreate->post_at = date_fmt_back($data["post_at"]);
            $postCreate->status = $data["status"];

            //upload cover
            if (!empty($_FILES["cover"])) {
                $files = $_FILES["cover"];
                $upload = new Upload();
                $image = $upload->image($files, $postCreate->title);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postCreate->cover = $image;
            }

            if (!$postCreate->save()) {
                $json["message"] = $postCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Agenda publicada com sucesso...")->flash();
            $json["redirect"] = url("/".PATH_ADMIN."/agend/agenda/{$postCreate->id}");

            echo json_encode($json);
            return;
        }

        //update
        if (!empty($data["action"]) && $data["action"] == "update") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $postEdit = (new Agenda())->findById($data["agenda_id"]);

            if (!$postEdit) {
                $this->message->error("Você tentou atualizar uma agenda que não existe ou foi removido")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/agend/home")]);
                return;
            }

            $postEdit->title = $data["title"];
            $postEdit->uri = str_slug($postEdit->title);
            $postEdit->event_date = date_fmt_back($data["event_date"]);
            $postEdit->city = $data["city"];
            $postEdit->local = $data["local"];
            $postEdit->details = $data["details"];
            // $postEdit->estate = $data["estate"];
            $postEdit->post_at = date_fmt_back($data["post_at"]);
            $postEdit->status = $data["status"];

            //upload cover
            if (!empty($_FILES["cover"])) {
                if ($postEdit->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->cover}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postEdit->cover}");
                    (new Thumb())->flush($postEdit->cover);
                }

                $files = $_FILES["cover"];
                $upload = new Upload();
                $image = $upload->image($files, $postEdit->title);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $postEdit->cover = $image;
            }

            if (!$postEdit->save()) {
                $json["message"] = $postEdit->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Agenda atualizada com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //delete
        if (!empty($data["action"]) && $data["action"] == "delete") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $postDelete = (new Agenda())->findById($data["agenda_id"]);

            if (!$postDelete) {
                $this->message->error("Você tentou excluir uma agenda que não existe ou já foi removido")->flash();
                echo json_encode(["reload" => true]);
                return;
            }

            if ($postDelete->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->cover}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$postDelete->cover}");
                (new Thumb())->flush($postDelete->cover);
            }

            $postDelete->destroy();
            $this->message->success("A agenda foi excluída com sucesso...")->flash();

            echo json_encode(["reload" => true]);
            return;
        }

        $postEdit = null;
        if (!empty($data["agenda_id"])) {
            $postId = filter_var($data["agenda_id"], FILTER_VALIDATE_INT);
            $postEdit = (new Agenda())->findById($postId);
        }

        $head = $this->seo->render(
            CONF_SITE_NAME . " | " . ($postEdit->title ?? "Nova Agenda"),
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/agend/agenda", [
            "app" => "agend/agenda",
            "head" => $head,
            "post" => $postEdit,
            "authors" => (new User())->findById(3)
        ]);
    }

}