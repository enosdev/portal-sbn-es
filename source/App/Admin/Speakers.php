<?php

namespace Source\App\Admin;

use Source\Models\Speaker;
use Source\Models\User;
use Source\Support\Pager;
use Source\Support\Thumb;
use Source\Support\Upload;

/**
 * Class Users
 * @package Source\App\Admin
 */
class Speakers extends Admin
{
    /**
     * Users constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function home(?array $data): void
    {
        //search redirect
        if (!empty($data["s"])) {
            $s = str_search($data["s"]);
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/speakers/home/{$s}/1")]);
            return;
        }

        $search = null;
        $speaker = (new Speaker())->find();

        if (!empty($data["search"]) && str_search($data["search"]) != "all") {
            $search = str_search($data["search"]);
            $speaker = (new Speaker())->find("MATCH(name, program) AGAINST(:s)", "s={$search}");

            if (!$speaker->count()) {
                $this->message->info("Sua pesquisa não retornou resultados")->flash();
                redirect("/".PATH_ADMIN."/speakers/home");
            }
        }

        $all = ($search ?? "all");
        $pager = new Pager(url("/".PATH_ADMIN."/speakers/home/{$all}/"));
        $pager->pager($speaker->count(), 12, (!empty($data["page"]) ? $data["page"] : 1));

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Locutores",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/speakers/home", [
            "app" => "speakers/home",
            "head" => $head,
            "search" => $search,
            "speakers" => $speaker->order("start_at")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render()
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function speaker(?array $data): void
    {
        //create
        if (!empty($data["action"]) && $data["action"] == "create") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $speakerCreate = new Speaker();
            $speakerCreate->name = $data["name"];
            $speakerCreate->program = $data["program"];
            $speakerCreate->start_at = $data["start_at"];
            $speakerCreate->end_at = $data["end_at"];
            $speakerCreate->status = $data["status"];
            $speakerCreate->week = implode(',',$data['week']);

            //upload photo
            if (!empty($_FILES["photo"])) {
                $files = $_FILES["photo"];
                $upload = new Upload();
                $image = $upload->image($files, $speakerCreate->name, 600);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $speakerCreate->photo = $image;
            }


            if (!$speakerCreate->save()) {
                $json["message"] = $speakerCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Locutor cadastrado com sucesso...")->flash();
            $json["redirect"] = url("/".PATH_ADMIN."/speakers/speaker/{$speakerCreate->id}");

            echo json_encode($json);
            return;
        }

        //update
        if (!empty($data["action"]) && $data["action"] == "update") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $speakerUpdate = (new Speaker())->findById($data["speaker_id"]);

            if (!$speakerUpdate) {
                $this->message->error("Você tentou gerenciar um locutor que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/speakers/home")]);
                return;
            }

            $speakerUpdate->name = $data["name"];
            $speakerUpdate->program = $data["program"];
            $speakerUpdate->start_at = $data['start_at'];
            $speakerUpdate->end_at = $data["end_at"];
            $speakerUpdate->status = $data["status"];
            $speakerUpdate->week = implode(',',$data['week']);

            //upload photo
            if (!empty($_FILES["photo"])) {
                if ($speakerUpdate->photo && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$speakerUpdate->photo}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$speakerUpdate->photo}");
                    (new Thumb())->flush($speakerUpdate->photo);
                }

                $files = $_FILES["photo"];
                $upload = new Upload();
                $image = $upload->image($files, $speakerUpdate->name, 600);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $speakerUpdate->photo = $image;
            }

            if (!$speakerUpdate->save()) {
                $json["message"] = $speakerUpdate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Locutor atualizado com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //delete
        if (!empty($data["action"]) && $data["action"] == "delete") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $speakerDelete = (new Speaker())->findById($data["speaker_id"]);

            if (!$speakerDelete) {
                $this->message->error("Você tentnou deletar um locutor que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/speakers/home")]);
                return;
            }

            if ($speakerDelete->photo && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$speakerDelete->photo}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$speakerDelete->photo}");
                (new Thumb())->flush($speakerDelete->photo);
            }

            $speakerDelete->destroy();

            $this->message->success("O locutor foi excluído com sucesso...")->flash();
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/speakers/home")]);

            return;
        }

        $speakerEdit = null;
        if (!empty($data["speaker_id"])) {
            $speakerId = filter_var($data["speaker_id"], FILTER_VALIDATE_INT);
            $speakerEdit = (new Speaker())->findById($speakerId);
        }

        $head = $this->seo->render(
            CONF_SITE_NAME . " | " . ($speakerEdit ? "Locutor {$speakerEdit->name}" : "Novo Locutor"),
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/speakers/speaker", [
            "app" => "speakers/speaker",
            "head" => $head,
            "speaker" => $speakerEdit
        ]);
    }
}