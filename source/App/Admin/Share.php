<?php

namespace Source\App\Admin;

use Source\Models\Category;
use Source\Models\Post;
use Source\Models\User;
use Source\Models\Gallery;
use Source\Support\Pager;
use Source\Support\Thumb;
use Source\Support\Upload;
use Source\Models\Faq\Channel;

/**
 * Class Share
 * @package Source\App\Admin
 */
class Share extends Admin
{
    /**
     * Share constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function home(?array $data): void
    {
        //search redirect
        if (!empty($data["s"])) {
            $s = str_search($data["s"]);
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/share/home/{$s}/1")]);
            return;
        }

        //category redirect
        if(!isset($_SESSION['cate'])){
            $_SESSION['cate'] = null;
        }
        if (!empty($data["c"])) {
            if($data['c'] == "null"){
                $_SESSION['cate'] = null;
            } else {
                $c = str_search($data["c"]);
                $_SESSION['cate'] = $c;
            }
            echo json_encode(["reload" => true]);
            return;
        }

        $where_cat = ($_SESSION['cate'] != null)? " && category = {$_SESSION['cate']}" : '';

        $search = null;
        $posts = (new Post())->find("type = :type && status != :st{$where_cat}",'type=post&st=trash');

        if (!empty($data["search"]) && str_search($data["search"]) != "all") {
            $search = str_search($data["search"]);
            $posts = (new Post())->find("MATCH(title, subtitle) AGAINST(:s){$where_cat} && status != :st", "s={$search}&st=trash");
            if (!$posts->count()) {
                $this->message->info("Sua pesquisa não retornou resultados")->flash();
                redirect("/".PATH_ADMIN."/share/home");
            }
        }

        $all = ($search ?? "all");
        $pager = new Pager(url("/".PATH_ADMIN."/share/home/{$all}/"));
        $pager->pager($posts->count(), 36, (!empty($data["page"]) ? $data["page"] : 1));

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Portal",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/share/home", [
            "app" => "share/home",
            "head" => $head,
            "posts" => $posts->limit($pager->limit())->offset($pager->offset())->order("post_at DESC")->fetch(true),
            "paginator" => $pager->render(),
            "search" => $search,
            "select_cate" => $_SESSION['cate'],
            "categories" => (new Category())->find("type = :type && parent IS NULL", "type=post")->order("title")->fetch(true),
        ]);
    }
}