<?php $v->layout('_theme');?>

<main class="main_content container">
    <div class="content">

        <div class="main_content_left container" style="width:100%; margin-bottom: 20px;">
            <article class="main_single_content" style="border-top-color:<?= $SiteColor; ?>;">
                <header style="border-bottom:none; text-align: center; margin-bottom: 0;">
                    <h1 style="font-weight:300;">Conteúdo não encontrado!</h1>
                </header>
                <div class="clear"></div>
                <div class="htmlchars align-center">
                    <p>Não encontramos conteúdo relacionado, mas não saia ainda. Fique informado! Veja nossas últimas notícias.</p>
                </div>

                <section class="main_outras_noticias container" style="border-top: 1px dotted #CCC; padding-top: 15px">
                    <h1 class="font-zero">Outras Notícias</h1>
                    <?php
                        if($maisNoticias):
                            foreach($maisNoticias as $news):
                                $v->insert("article_news", ["news" => $news]);
                            endforeach;
                        endif;
                    ?>
                    <div class="clear"></div>
                </section><!-- Outras Notícias -->
            </article><!-- Content -->
        </div><!-- CONTENT FULL -->
        <div class="clear"></div>
    </div>
</main>