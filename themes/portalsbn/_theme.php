<?php

    // $mostrar = false;
    // $ip = $_SERVER['REMOTE_ADDR'];
    // $url = file_get_contents("http://ip-api.com/json/{$ip}");
    // $region = json_decode($url)->countryCode;
    // if(!empty($region)){
    //     if($region != "BR"):
    //         $mostrar = true;
    //         $mensagemAds = '<div style="position:fixed;top:50px;left:0;padding:10px;color:#fff;background-color:red;z-index:9999">ads min</div>';
    //     endif;
    // }

    // $SiteColor = '#0888CB';
    $SiteColor = $cor ?? '#0888CB';
    $selectEstate = '<a class="main_header_topo_estados" title="Selecione um estado" href="'.CONF_URL_STATE.'">
                    <img src="'.theme("/img/main_header_es.png").'" alt="Espírito Santo" title="Espírito Santo"/>
                    Espírito Santo</a>';
    // $categoryUri = filter_var($data["category"], FILTER_SANITIZE_STRIPPED);
    // $category = (new Source\Models\Category())->findByUri($categoryUri);
    // echo $cor;

    
    
    $category_menu = (new Source\Models\Category())
                ->find('parent IS NULL && menu = :m', 'm=on')
                ->fetch(true);

    $dezDias =  date('Y-m-d H:i:s', strtotime('-10days'));
    $ultimas_menu = (new Source\Models\Post())
                ->findPost('type = :t && create_at > :c', 't=post&c='.$dezDias)
                ->order("post_at DESC")
                ->limit(4)
                ->fetch(true);
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>

    <meta charset="UTF-8">
    <meta name="mit" content="2019-03-20T00:16:42-03:00+12918">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <?=$head;?>

    <link rel="site" href="<?= url(); ?>">
    <link rel="stylesheet" href="<?= theme("/css/programmerboot.css?v=".CONF_SITE_VERSION); ?>"/>
    <link rel="stylesheet" href="<?= theme("/css/style.css?v=".CONF_SITE_VERSION); ?>">
    <link rel="stylesheet" href="<?= theme("/css/datetimepicker.css?v=".CONF_SITE_VERSION); ?>"/>
    <link rel="stylesheet" href="<?= theme("/_cdn/jquery.fancybox.css?v=".CONF_SITE_VERSION); ?>"/>
    <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700,800|Merriweather:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:200,400,800|Merriweather:200,400,700" rel="stylesheet">
    <link rel="stylesheet" href="<?= theme("/css/font-awesome.css?v=".CONF_SITE_VERSION); ?>"/>
    <link rel="shortcut icon" href="<?= theme("/img/icon.png?v=".CONF_SITE_VERSION); ?>"/>

    <script src="<?= theme("/_cdn/jquery.js?v=".CONF_SITE_VERSION); ?>"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <?php
        echo google_tags(['G-W2SESX1K0W','UA-29045980-1']);
    ?>
    
    <?php
        echo adsense('ca-pub-4340959551899971');
    ?>

    <!-- Código de Integração https://www.portalsbn.com.br/ EUREKA -->
    <script type="text/javascript"> var EUREKAD = EUREKAD || {_setAccount: "d0f803d5f35af71f95b36983965020e3_2106"}; </script>
    <script async src="https://tag.ageureka.com/eureka_ads.js?v=0.0.2"></script>
</head>
<body>
    <?php
         echo adsense('ca-pub-4340959551899971', true);
    ?>
    <header class="main_header container">
        <div class="main_header_content container" style="background: <?= $SiteColor; ?>;">

            <div class="main_header_topo container">
                <div class="content">
                    <a class="main_header_topo_data" title=""> <span id="dia"></span> <span id="hora"></span></a>
                    <?= $selectEstate; ?>
                    <div class="main_header_topo_social">
                        <p>Siga-nos</p>
                        <ul class="social">
                            <li class="social_item"><a class="facebook" href="https://www.facebook.com/<?= CONF_SOCIAL_FACEBOOK_PAGE; ?>" target="_blank" rel="nofollow" title="Portal SBN no Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li class="social_item"><a class="twitter" href="https://www.twitter.com/<?= CONF_SOCIAL_TWITTER_CREATOR; ?>" target="_blank" rel="nofollow" title="Portal SBN no Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li class="social_item"><a class="youtube" href="https://www.youtube.com/<?= CONF_SOCIAL_YOUTUBE_PAGE; ?>" target="_blank" rel="nofollow" title="Portal SBN no Youtube"><i class="fa fa-youtube"></i></a></li>
                            <li class="social_item"><a class="instagram" href="https://www.instagram.com/<?=CONF_SOCIAL_INSTAGRAM_PAGE;?>" target="_blank" rel="nofollow" title="Portal SBN no Instagram"><i class="fa fa-instagram"></i></a></li>
                            <li class="social_item"><a class="telegram" href="<?=CONF_SOCIAL_TELEGRAM;?>" target="_blank" rel="nofollow" title="Portal SBN no Telegram"><i class="fa fa-telegram"></i></a></li>
                            <li class="social_item"><a class="whatsapp" href="<?=CONF_SOCIAL_WHATSAPP;?>" target="_blank" rel="nofollow" title="Portal SBN no Telegram"><i class="fa fa-whatsapp"></i></a></li>
                        </ul>
                    </div>
                    <div class="divclear"></div>
                </div>
            </div><!-- Topo -->
            
            <div class="content margin-top-10 margin-bottom-10" style="position: relative;">
                <div class="main_header_logo">
                    <a href="<?= url(); ?>" title=""><img src="<?= theme("/assets/img/header-logo.png");?>" alt="" title=""/></a>
                </div>
                <aside class="main_banner_728">
                    <!-- <img src="< ?= theme('/img/banner_728_90.png');?>" alt=""> -->
                    <?= bannerAds(1,728,90); ?>
                </aside>

                <div class="main_mobile_action round"><i class="fa fa-navicon"></i></div>
                <div class="clear"></div>
            </div>


            <!-- menu mobile -->
            <ul class="main_header_nav box-shadow">
                <form class="main_header_nav_search" method="post" name="search" action="<?= url("/artigo/buscar"); ?>">
                    <input type="text" name="s" class="search" placeholder="Buscar no SBN"/>
                    <button type="submit" class="button"><i class="fa fa-search"></i></button>
                </form>
                <?php
                    $ettMobile = ["entretenimento", "evento", "agenda", "coberturas", "fotos", "divulgue-seu-evento", "solicitar-cobertura"];
                    $ulMobile = explode('/',filter_input(INPUT_GET, "route", FILTER_SANITIZE_STRIPPED));
                    $ulMobile = $ulMobile[1] ?? '';
                    if( in_array($ulMobile, $ettMobile) ):?>
                        <li class="main_header_nav_item <?= ( $ul == 'entretenimento' ? "active" : "" ); ?>"><a href="<?= url("/entretenimento"); ?>" title="Entretenimento">Entretenimento</a></li>
                        <li class="main_header_nav_item <?= ( $ul == 'coberturas' ? "active" : "" ); ?>"><a href="<?= url("/coberturas"); ?>" title="Coberturas de Eventos">Coberturas</a></li>
                        <li class="main_header_nav_item <?= ( ($ul == 'agenda' || $ul == 'evento') ? "active" : "" ); ?>"><a href="<?= url("/agenda"); ?>" title="Próximos Eventos">Agenda</a></li>
                        <li class="main_header_nav_item <?= ( $ul == 'divulgue-seu-evento' ? "active" : "" ); ?>"><a href="<?= url("/divulgue-seu-evento"); ?>" title="Divulgue seu Evento">Divulgue seu Evento</a></li>
                        <li class="main_header_nav_item <?= ( $ul == 'solicitar-cobertura' ? "active" : "" ); ?>"><a href="<?= url("/solicitar-cobertura"); ?>" title="Solicitar Cobertura">Solicitar Cobertura</a></li>
                        <li class="main_header_nav_item"><a href="<?= url(); ?>" title="Solicitar Cobertura">Notícias</a></li>
                <?php else: ?>
                    <li class="main_header_nav_item"><a href="<?=url();?>">Últimas</a></li>
                    <?php
                        //carrega o menu no site
                        if($category_menu):
                            foreach($category_menu as $c):
                                $v->insert("header_menu_mobile.php", ["menu" => $c]);
                                // if($c->uri != 'entretenimento'):
                                //     $v->insert("header_menu.php", ["menu" => $c]);
                                // else:
                                //     $v->insert("header_menu_ent.php", ["menu" => $c]);
                                // endif;
                            endforeach;
                        endif;
                    endif; ?>
            </ul>
            <!-- /menu mobile -->


            <ul id="menuHeader" class="main_header_nav_desktop container">
            <?php
                $ett = ["entretenimento", "evento", "agenda", "coberturas", "fotos", "divulgue-seu-evento", "solicitar-cobertura"];
                $ul = explode('/',filter_input(INPUT_GET, "route", FILTER_SANITIZE_STRIPPED));
                $ul = $ul[1] ?? '';
                if( in_array($ul, $ett) )
                {?>

                <li class="main_header_nav_desktop_item <?= ( $ul == 'entretenimento' ? "active" : "" ); ?>"><a href="<?= url("/entretenimento"); ?>" title="Entretenimento">Entretenimento</a></li>
                <li class="main_header_nav_desktop_item <?= ( $ul == 'coberturas' ? "active" : "" ); ?>"><a href="<?= url("/coberturas"); ?>" title="Coberturas de Eventos">Coberturas</a></li>
                <li class="main_header_nav_desktop_item <?= ( ($ul == 'agenda' || $ul == 'evento') ? "active" : "" ); ?>"><a href="<?= url("/agenda"); ?>" title="Próximos Eventos">Agenda</a></li>
                <li class="main_header_nav_desktop_item <?= ( $ul == 'divulgue-seu-evento' ? "active" : "" ); ?>"><a href="<?= url("/divulgue-seu-evento"); ?>" title="Divulgue seu Evento">Divulgue seu Evento</a></li>
                <li class="main_header_nav_desktop_item <?= ( $ul == 'solicitar-cobertura' ? "active" : "" ); ?>"><a href="<?= url("/solicitar-cobertura"); ?>" title="Solicitar Cobertura">Solicitar Cobertura</a></li>
                <li class="main_header_nav_desktop_item"><a href="<?= url(); ?>" title="Solicitar Cobertura">Notícias</a></li>
                    
               <?php }else{ ?>

                <li class="main_header_nav_desktop_item <?=$menuHome ?? '';?>"><a href="<?=url();?>">Últimas</a>
                    <ul class="main_header_nav_desktop_sub">
                        <div class="content">
                            <?php foreach($ultimas_menu as $ultimas):?>
                                <article class="main_box_news main_box_white item">
                                    <a href="<?= url("/artigo/{$ultimas->uri}");?>" title="<?=$ultimas->title;?>">
                                        <img class="lazyload" data-src="<?= image($ultimas->cover, 480,240); ?>" alt="<?=$ultimas->title;?>" title="<?=$ultimas->title;?>"/>
                                    </a>
                                    <div class="main_box_news_desc" style="min-height: auto;">
                                        <ul class="social">
                                            <li class="social_item"><a href="https://api.whatsapp.com/send?text=Veja esta matéria:<?= url("/artigo/{$ultimas->uri}");?>" title="Compartilhe WhatsApp" onclick="window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;" class="whatsapp" rel="nofollow"><i class="fa fa-whatsapp"></i></a></li>
                                            <li class="social_item"><a href="http://www.facebook.com/sharer.php?u=<?= url("/artigo/{$ultimas->uri}");?>" title="Compartilhe no Facebook" onclick="window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;" class="facebook" rel="nofollow"><i class="fa fa-facebook"></i></a></li>
                                            <li class="social_item"><a href="https://twitter.com/intent/tweet?url=<?= url("/artigo/{$ultimas->uri}");?>&text=<?=$ultimas->title;?>" title="Conte isto no Twitter" onclick="window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;" class="twitter" rel="nofollow"><i class="fa fa-twitter"></i></a></li>
                                        </ul><!-- social share -->

                                        <a href="<?= url("/artigo/{$ultimas->uri}");?>" title="<?=$ultimas->title;?>">
                                            <mark style="background-color:<?= $ultimas->category()->color;?>" class="categoria"><?= $ultimas->category()->title;?></mark>
                                            <p class="tagline" style="color:#666;"><?=$ultimas->tag;?></p>
                                            <time datetime="<?= date('Y-m-d H:i:s', strtotime($ultimas->post_at));?>" style="margin-top: 4px;"><?= date('d/m/Y', strtotime($ultimas->post_at));?></time>
                                            <div class="clear"></div>

                                            <h1><?=str_limit_chars($ultimas->title, 70);?></h1>
                                        </a>
                                    </div>
                                </article>
                            <?php endforeach;?>
                        </div>
                    </ul>
                </li>
                <?php
                    //carrega o menu no site
                    if($category_menu):
                        foreach($category_menu as $c):
                            if($c->uri != 'entretenimento'):
                                $v->insert("header_menu.php", ["menu" => $c]);
                            else:
                                $v->insert("header_menu_ent.php", ["menu" => $c]);
                            endif;
                        endforeach;
                    endif;
                ?>
                <button class="main_header_nav_search_desktop_button j_search_button" type="submit"><i class="fa fa-search"></i></button>
                <div class="clear"></div>
                <?php }?>
                <!-- finaliza o menu separado -->
            </ul>


        </div><!-- HEADER CONTENT -->
    </header><!-- Header -->
    <div class="dashboard_search_container j_search_form">
        <div class="dashboard_search">
            <div class="dashboard_search_bg"></div>
            <div class="dashboard_search_form">
                <form name="search" method="post"  action="<?= url("/artigo/buscar"); ?>">
                    <input type="text" class="j_search_focus" name="s" placeholder="Buscar no SBN" autocomplete="off" required/>
                    <button><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>
    </div><!-- SEARCH -->

    <!-- CONTENT -->
    <?= $v->section("content"); ?>


    <footer class="main_footer">
        <button class="main_btn_topo"><i class="fa fa-chevron-up"></i></button>

        <div class="main_footer_copy">
            <p><img src="<?= theme("/assets/img/footer-logo.png"); ?>" alt="Portal SBN" title="Portal SBN"/> © 2011 - <?=date("Y");?> Portal CBN - Todos os direitos reservados</p>
        </div>
        <nav class="main_footer_nav">
            <h1 class="font-zero">Sobre o Portal CBN</h1>
            <ul>
            <?php
            $pag = (new Source\Models\Post())
            ->findPost('type = :t', 't=page')
            ->order("title")
            ->fetch(true);
            if($pag):
                foreach($pag as $p):?>
                    <li><a href="<?=url("/pagina/{$p->uri}");?>" title="<?=$p->title;?>"><?=$p->title;?></a></li>
                <?php endforeach;
            else:
                echo '<li><a href="#" title="link">link</a></li>';
            endif;
            ?>
            </ul>
        </nav>
    </footer>

    
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v5.0"></script>

<script src="<?= theme("/_cdn/jmask.js?v=".CONF_SITE_VERSION); ?>"></script>
<script src="<?= theme("/_cdn/html5.js?v=".CONF_SITE_VERSION); ?>"></script>
<script src="<?= theme("/_cdn/programmer.js?v=".CONF_SITE_VERSION); ?>"></script>
<script src="<?= theme("/_cdn/sharebox.js?v=".CONF_SITE_VERSION); ?>"></script>
<script src="<?= theme("/_cdn/jquery.fancybox.min.js?v=".CONF_SITE_VERSION); ?>"></script>
<script src="<?= theme("/_cdn/jquery.countdown.js?v=".CONF_SITE_VERSION); ?>"></script>
<script src="<?= theme("/_cdn/jquery.form.js?v=".CONF_SITE_VERSION); ?>"></script>
<script src="<?= theme("/_cdn/datetimepicker.js?v=".CONF_SITE_VERSION); ?>"></script>
<script src="<?= theme("/assets/js/script.js?v=".CONF_SITE_VERSION); ?>"></script>
<script src="<?= theme("/assets/js/lazysizes.min.js?v=".CONF_SITE_VERSION); ?>"></script>

<?=bannerAds(99);?>

<div style="position:fixed;left:1%;bottom:1%;z-index:-1;opacity: 0"><div data-pid="rxwqdvxxbmfyi" data-t="i" data-mt="b" data-render-delay="0" data-iframe-class="" data-iframe-style="" data-pu-fallback="1"></div>
<script id="rxwqdvxxbmfyi" type="text/javascript"> (function (document, window) { document.addEventListener('DOMContentLoaded', function (event) { let a = document.createElement("script"); a.type = 'text/javascript'; a.async = true; a.src = "https://data.gblcdn.com/data/pastoclockp.js?aid=6f4889f8e38ca8e79c6a&pubid=54af39e0-7af7-11ea-918f-3585c73dba46&pid=rxwqdvxxbmfyi&renderD=0&limitT=0&limitH=1&parent=body&t=i&mt=b"; document.body.appendChild(a); document.getElementById('rxwqdvxxbmfyi').remove(); }); }) (document, window); </script>

</body>
</html>