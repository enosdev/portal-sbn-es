<div class="eureka_vertical"></div>

<section class="main_ultimas container">
    <header>
        <h1>Últimas <span class="font-zero">Notícias</span></h1>
        <ul>
            <li class="main_ultimas_nav main_ultimas_nav_ba active">Bahia</li>
            <li class="main_ultimas_nav main_ultimas_nav_es ">Espírito Santo</li>
        </ul>
    </header>
    <section class="main_ultimas_content main_ultimas_ba container first">
        <h1 class="font-zero">Bahia</h1>
        <?php
            $es = file_get_contents("https://www.portalsbn.com/bahia/520/250");
            $api_es = json_decode($es);
            foreach($api_es as $value):
        ?>
            <article class="main_ultimas_item">
                <a href="<?=$value->uri;?>" title="<?=$value->title;?>">
                    <img class="lazyload" data-src="<?=$value->cover;?>" title="<?=$value->title;?>" alt="<?=$value->title;?>">
                    <h1><?=str_limit_words($value->title,12);?></h1>
                </a>
            </article>
        <?php endforeach;?>
        <div class="clear"></div>
    </section><!-- bahia -->

    <section class="main_ultimas_content main_ultimas_es container">
        <h1 class="font-zero">Espírito Santo</h1>
        <?php if($ultimasEstado):
            foreach($ultimasEstado as $us):?>
            <article class="main_ultimas_item">
                <a href="<?=url("/artigo/{$us->uri}");?>" title="<?=$us->title;?>">
                    <img src="<?=image($us->cover, 520, 250);?>" title="<?=$us->title;?>" alt="<?=$us->title;?>">
                    <h1><?=$us->title;?></h1>
                </a>
            </article>
        <?php endforeach;
        endif;?>
        <div class="clear"></div>
    </section><!-- espírito santo -->
    <div class="clear"></div>
</section><!-- ÚLTIMAS BA ES -->

<div class="margin-bottom-20 eureka_vertical"></div>

<aside class="main_banner_300 margin-bottom-20 banner">
    <h1 class="font-zero">Publicidade</h1>
    <!-- <img src="< ?= theme('/img/banner_300_600.png');?>" alt=""> -->
    <?= bannerAds(2,300,250); ?>
</aside><!-- banner 300 -->

<style>
    .box-maislidas .mais-lidas{
        border-bottom: solid 1px #ccc;
        margin-bottom:10px;
    }
    .box-maislidas article li{
        display:flex;
        align-items:center;
        margin: 10px 0;
        border-bottom: solid 1px #ccc;
        padding-bottom:10px
    }
    .box-maislidas article li:nth-last-child(1){
        border-bottom:none
    }
    .box-maislidas article span{
        font-size: 4em;
        margin-right:10px
    }
    .box-maislidas article a{
        color:#333;
        font-size:1.2em
    }
</style>
<section class="container box-maislidas">
    <h1 class="mais-lidas">As mais lidas do mês</h1>
    <article>
        <ul>
        <?php
        if($maislidas):
            $i = 1; foreach($maislidas as $lidas):?>
                <li><span style="color:<?=$lidas->category()->color;?>"><?=$i++;?>.</span><a href="<?=url("/artigo/{$lidas->uri}");?>"><?=$lidas->title;?></a></li>
            <?php endforeach;
        else:
            echo 'sem postagens neste mês';
        endif;?>
        </ul>
    </article>
</section>
<aside class="main_banner_300 margin-bottom-20 banner">
    <h1 class="font-zero">Publicidade</h1>
    <!-- <img src="< ?= theme('/img/banner_300_600.png');?>" alt=""> -->
    <?= bannerAds(3,300,250); ?>
</aside><!-- banner 300 -->