<?php
    if($enquete):
?>
    <section class="pc_enquete margin-bottom-20">
        <h1 style="<?=$display ?? '';?>"><?= $enquete->channel; ?></h1>
        <div class="enquete_form j_enquete_form <?=$class_none2 ?? '';?>">
        <!-- < div class="enquete_form j_enquete_form < ?= ( isset($_COOKIE['enquete-' . $readEnquete->getResult()[0]['enquete_id']]) ? "display-none" : "" ); ?>"> -->
            <form name="enquete" class="j_enquete" method="post" action="">
                <?php
                if ($enquete->questions()->fetch()):
                    $totalVotes = 0;
                    foreach ($enquete->questions()->fetch(true) as $Ques):
                        //Soma os votos para calculo do percentual "enquete_results"
                        $totalVotes += $Ques->votes;
                        ?>
                        <article>
                            <h1 class="display-none"><?= $Ques->question; ?></h1>
                            <input type="hidden" name="channel_id" value="<?= $enquete->id; ?>"/>
                            <label>
                                <input type="radio" class="j_enquete_question" name="question_id" value="<?= $Ques->id; ?>" required>
                                <span class=""><?= $Ques->question; ?></span>
                            </label>
                        </article>
                        <?php
                    endforeach;
                endif;
                ?>
                <div class="<?=$class_none2 ?? '';?>">Escreva <strong style="color:red;font-size:1.2em"><?=captcha();?></strong> abaixo</div>
                <input style="max-width:100px;margin-bottom:5px;border:solid 1px #ccc" class="<?=$class_none2 ?? '';?>" type="text" name="captcha" required>
                <button class="btn btn-blue font-large <?=$class_none2 ?? 'j_enquete_action';?>">Votar</button>
                <a href="javascript:void(0)" class="view_results j_view_results" title="Ver resultados">Ver resultados</a>
            </form>
        </div>

        <div class="enquete_results j_enquete_results <?=$class_none ?? 'display-none';?>">
        <!-- < div class="enquete_results j_enquete_results < ?= ( isset($_COOKIE['enquete-' . $readEnquete->getResult()[0]['enquete_id']]) ? "" : "display-none" ); ?>"> -->
            <?php
            if ($enquete->questions()->fetch()):
                foreach ($enquete->questions()->fetch(true) as $Ques):
                    //Calcula o percentual dos votos
                    //$percent = ( $Ques->votes == 0 ? 0 : round($Ques->votes / $totalVotes * 100) );
                    $percent = ( $Ques->votes == 0 ? 0 : ($Ques->votes * 100) / $totalVotes);
                    ?>
                    <article>
                        <h1><?= $Ques->question; ?> - (<?= number_format($percent,2,',',''); ?>% | <?= $Ques->votes; ?> Votos)</h1>
                        <div class="percent_bar">
                            <div class="percent" style="width: <?= $percent ?>%;"></div>
                        </div>
                    </article>
                    <?php
                endforeach;
            endif;
            ?>
            <p class="total_votes">Total de votos: <?= $totalVotes; ?></p>
            <br>
            <a id="voltar" href="javascript:void(0)" class="view_results j_view_results <?=$class_none2 ?? '';?>" title="Voltar para votar"><i class="fa fa-chevron-left"></i> Voltar</a>
            <p id="javotou" style="color:#707070;text-align:center"></p>
            <!-- <a href="javascript:void(0)" class="view_results j_view_results < ?= ( isset($_COOKIE['enquete-' . $readEnquete->getResult()[0]['enquete_id']]) ? "display-none" : "" ); ?>" title="Inserir meu voto"><i class="fa fa-chevron-left"></i> Votar</a> -->
        </div>
        <div style="clear:both"></div>
        <div style="text-align:center;margin-top:5px">
        <p id="javotou" class="javotou" style="color:#707070;text-align:center"></p>
        <!-- < div style="text-align:center;margin-top:5px" class=" < ?= ( isset($_COOKIE['enquete-' . $readEnquete->getResult()[0]['enquete_id']]) ? "display-none" : "" ); ?>"> -->
        <p style='color:red;padding-top:10px' class="time_enquete j_clock_enquete" datetime="<?= date('Y/m/d H:i:s', strtotime($enquete->expire_at)); ?>"></p>
        </div>
    </section>
<?php 
    endif;
?>
<script>
    //null todas informações
    


    let getPoll = localStorage.getItem("WkdjWgd8df#jdsnHFn4<?=$enquete->id ?? 0;?>")
    if (getPoll){
    //     console.log('não existe')
    //     let get = { id: '0', time: <?=time();?>, ip: '2001:12ff:0:4::22' };
    //     localStorage.setItem('WkdjWgd8df#jdsnHFn4', JSON.stringify(get));
    // } else {

        let parse = JSON.parse(getPoll)
        if ( parse.id == <?=$enquete->id ?? 0;?> ){
            if ( parse.ip == '<?=$_SERVER['REMOTE_ADDR'];?>' ){

                if ( parse.time > <?=time();?> ){

                    let channel = document.querySelector(".j_enquete_form")
                    channel.classList.add("display-none")

                    let res = document.querySelector(".j_enquete_results")
                    res.classList.remove("display-none")
                    
                    let voltar = document.querySelector('#voltar')
                    voltar.classList.add('display-none')
                    document.querySelector('#javotou').innerText = 'Você já votou nessa enquete'
                } else {
                    localStorage.removeItem('WkdjWgd8df#jdsnHFn4<?=$enquete->id ?? 0;?>')
                }

            }
        }

    }
    
</script>