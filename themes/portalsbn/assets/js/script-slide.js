let str = window.location.pathname
let substr = "/entretenimento"
let resultStr = str.indexOf(substr) > -1

if (resultStr == true) {

    let slide = document.querySelectorAll('.slide .conteudo')
    // let altura = slide[0].clientHeight + 1
    // slide[0].style.display = 'block'

    // cria os indices
    let span = document.createElement('span')
    let indicador = document.querySelector('.indicador')
    for (let i = 1; i <= slide.length; i++) {
        indicador.append(span)
        span = document.createElement('span')
    }

    let indice = indicador.querySelectorAll('span')
    let progress = document.querySelector('.progressbar .progress')

    // ao clicar no link do indice levar ao slide referente
    var width = 0
    var e = 0
    for (let li = 0; li < slide.length; li++) {
        indice[li].addEventListener('click', () => {
            e = li
            width = 0
            indice[li].classList.add("active")
            slide[li].classList.add("active")

            let altura = slide[li].clientHeight + 1
            document.querySelector('.slider').style.height = `${altura}px`

            for (let rm = 0; rm < slide.length; rm++) {
                if (rm === li) {
                    continue
                }
                clearInterval(intesss)
                indice[rm].classList.remove("active")
                slide[rm].classList.remove("active")
            }
        })
    }

    // fazer o slide ficar automático
    var intesss = null
    start = () => {
        intesss = setInterval(() => {
            indice[e].classList.add("active")
            slide[e].classList.add("active")

            let altura = slide[e].clientHeight + 1
            document.querySelector('.slider').style.height = `${altura}px`

            progress.style.width = width++ + "%";
            if (width == 101) {
                width = 0
                indice[e].classList.remove("active")
                slide[e].classList.remove("active")
                e++
                if (e == slide.length) {
                    e = 0
                }
            }
        }, 60)
    }


    //inicia o slide
    start()

    // ao passor o mouse sobre o slide da pause e ao retirar continua
    var elementos = document.querySelector('.slide')
    elementos.addEventListener('mouseover', () => clearInterval(intesss))
    elementos.addEventListener('mouseout', start)

}