<?php $v->layout("_theme");?>

<main class="main_content container">
    <div class="content">

        <div class="main_content_left container" style="width:100%; margin-bottom: 20px;">
            <article class="main_single_content" style="border-top-color:#0888CB;">
                <header style="border-bottom:none; margin-bottom: 0;">
                    <h1 style="font-weight:300;"><?=$title;?></h1>
                </header>
                <div class="clear"></div>
                <div class="htmlchars">
                    <p>Sua pesquisa por <?=$search;?> obteve <?=$conta;?> resultado(s).</p>
                </div>

                <section class="main_outras_noticias container" style="border-top: 1px dotted #CCC; padding-top: 15px">
                    <h1 class="font-zero">Outras Notícias</h1>
                    <?php
                        if($article):
                            foreach($article as $news):
                                $v->insert("article_news", ["news" => $news]);
                            endforeach;
                        endif;
                    ?>
                    <div class="clear"></div>
                </section><!-- Outras Notícias -->
                <?=$paginator;?>
            </article><!-- Content -->
        </div><!-- CONTENT FULL -->
        <div class="clear"></div>
    </div>
</main>