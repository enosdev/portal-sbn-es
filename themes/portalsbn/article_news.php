<?php
$linkEntretenimento = ($news->category()->id == 36 || $news->category()->id == 10 || $news->category()->parent == 10)? 'artigos-entretenimento' : 'artigo' ;
?>
<?php $icon = ( strpos($news->content, "youtube.com/embed/") ? "<div style=\"top:-17px; bottom:auto;\" class=\"main_midia_icon\">Vídeo <i class=\"fa fa-youtube-play\"></i></div>" : (strpos($news->content, "@#[") ? "<div style=\"top:-17px; height:18px; background: #0071BC;\" class=\"main_midia_icon\">Galeria <i class=\"fa fa-picture-o\"></i></div>" : "") );?>
<article class="main_box_news main_box_white item">
    <a href="<?= url("/{$linkEntretenimento}/{$news->uri}");?>" title="<?=$news->title;?>">
        <img class="img-fluid lazyload" data-src="<?= image($news->cover, 480,240); ?>" alt="<?=$news->title;?>" title="<?=$news->title;?>"/>
    </a>

    <div class="main_box_news_desc">
        <ul class="social">
            <li class="social_item"><a href="https://api.whatsapp.com/send?text=Veja esta matéria:<?= url("/{$linkEntretenimento}/{$news->uri}"); ?> - É notícia? Tá no <?=CONF_SITE_NAME;?> | <?=CONF_SITE_TITLE;?>!" title="Compartilhe WhatsApp" onclick="window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;" class="whatsapp" rel="nofollow"><i class="fa fa-whatsapp"></i></a></li>
            <li class="social_item"><a href="http://www.facebook.com/sharer.php?u=<?= url("/{$linkEntretenimento}/{$news->uri}"); ?>" title="Compartilhe no Facebook" onclick="window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;" class="facebook" rel="nofollow"><i class="fa fa-facebook"></i></a></li>
            <li class="social_item"><a href="https://twitter.com/intent/tweet?url=<?= url("/{$linkEntretenimento}/{$news->uri}"); ?>&text=<?=$news->title;?>" title="Conte isto no Twitter" onclick="window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;" class="twitter" rel="nofollow"><i class="fa fa-twitter"></i></a></li>
        </ul>
        
        <a href="<?=url("/{$linkEntretenimento}/{$news->uri}");?>" title="<?=$news->title;?>">
            <?=$icon;?>
            <mark style="background-color:<?= $news->category()->color; ?>;" class="categoria"><?=$news->category()->title;?></mark>
            <p class="tagline"><?=$news->tag;?></p>
            <time datetime="<?= $news->post_at;?>"><?= date('d/m/Y', strtotime($news->post_at));?></time>
            <div class="clear"></div>
            <h1><?=str_limit_chars($news->title, 70);?></h1>
        </a>
    </div>
</article>