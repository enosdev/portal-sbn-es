<?php
$linkEntretenimento = ($sl->category()->id == 36 || $sl->category()->id == 10 || $sl->category()->parent == 10)? 'artigos-entretenimento' : 'artigo' ;
if($mod == 'mobile'):
    $icon = ( strpos($sl->content, "youtube.com/embed/") ? "<div style=\"top:-18px; bottom:auto;\" class=\"main_midia_icon\">Vídeo <i class=\"fa fa-youtube-play\"></i></div>" : (strpos($sl->content, "@#[") ? "<div style=\"top:-17px; bottom:auto;\" class=\"main_midia_icon\">Galeria <i class=\"fa fa-picture-o\"></i></div>" : "") );
?>

<article class="main_box_news main_box_gray slide_mobile_item slide_mobile_item_destaques<?=$first;?>">
    <a href="<?= url("/{$linkEntretenimento}/{$sl->uri}"); ?>" title="<?=$sl->title;?>">
        <img class="img-fluid lazyload" data-src="<?= image($sl->cover, 500); ?>" alt="<?=$sl->title;?>" title="<?=$sl->title;?>">
    </a>
    <div class="main_box_news_desc">
        <?=$icon;?>
        <a href="<?= url("/{$linkEntretenimento}/{$sl->uri}"); ?>" title="<?=$sl->title;?>">
            <mark style="background-color:<?= $sl->category()->color; ?>; " class="categoria"><?= $sl->category()->title; ?></mark>
            <p class="tagline"><?= $sl->category()->tag; ?></p>
            <time datetime="<?= date('Y-m-d H:i:s', strtotime($sl->post_at));?>"><?= date('d/m/Y', strtotime($sl->post_at));?></time>
            <div class="clear"></div>
            <h1><?= str_limit_chars($sl->title, 80); ?></h1>
        </a>
    </div>
</article><!-- Item Mobile -->

<?php 
elseif($mod == 'normal'): ?>
<div class="main_slide_destaques_item slide_item_destaques<?= $first; ?>">
    <a href="<?=url("/{$linkEntretenimento}/{$sl->uri}");?>" title="<?=$sl->title;?>">
        <img class="lazyload" data-src="<?= image($sl->cover, 840, 605); ?>" alt="<?=$sl->title;?>" title="<?=$sl->title;?>">
    </a>
    <div class="main_slide_destaques_item_desc">
        <a href="<?=url("/{$linkEntretenimento}/{$sl->uri}");?>" title="<?=$sl->title;?>">
            <mark style="background-color:<?= $sl->category()->color; ?>;" class="categoria"><?= $sl->category()->title; ?></mark>
            <p class="tagline"><?=$sl->tag;?></p>
            <time datetime="<?= date('Y-m-d H:i:s', strtotime($sl->post_at));?>"><?= date('d/m/Y', strtotime($sl->post_at));?></time>
            <div class="clear"></div>
            <p class="title"><?=$sl->title;?></p>
        </a>
    </div>
</div>
<?php
elseif($mod == 'link'):
    $icon = ( strpos($sl->content, "youtube.com/embed/") ? "<div class=\"main_midia_icon\">Vídeo <i class=\"fa fa-youtube-play\"></i></div>" : (strpos($sl->content, "@#[") ? "<div class=\"main_midia_icon\">Galeria <i class=\"fa fa-picture-o\"></i></div>" : "") );
?>

<div class="main_slide_destaques_item_click slide_mark_destaques<?= $active; ?>" id="<?= ($i-1); ?>">
    <mark style="background-color:<?= $sl->category()->color; ?>; " class="categoria"><?= $sl->category()->title; ?></mark>
    <p class="tagline"><?=$sl->tag;?></p>
    <div class="clear"></div>
    <p class="title"><?=str_limit_chars($sl->title, 80);?></p>
    <?=$icon;?>
</div><!-- Item click -->

<?php
endif;