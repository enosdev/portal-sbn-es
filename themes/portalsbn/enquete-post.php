<?php $v->layout("_theme"); ?>
<!-- modificação feita por Enos -->
<!-- CARREGA TODO CSS DA PARTE DE COMPARTILHAMENTO DE MATÉRIAS -->
<style>
    .right{float:right}
    .display{display:none}
    .article-header-info {display: block;border-top: 1px dotted #bfbfbf;padding-top: 20px;}
    .article-header-info .article-header-meta:after {display: block;content: '';clear: both;}
    .article-header-info .article-header-meta > span {display: block;float: left;color: #2a2b2c;margin-right: 10px;}
    .article-header-info .article-header-meta > span:last-child {margin-right: 0px;}
    .article-header-info .article-header-meta .article-header-meta-date {font-size: 30px;font-weight: bold;line-height: 100%;}
    .article-header-info .article-header-meta .article-header-meta-time .head-time {font-size: 12px;line-height: 100%;display: block;padding-top: 2px;}
    .article-header-info .article-header-meta .article-header-meta-time .head-year {font-size: 13px;line-height: 100%;font-weight: bold;display: block;padding-top: 1px;}
    .article-header-info .article-header-meta .article-header-meta-links {border-left: 1px dotted #bfbfbf;margin-left: 5px;padding-left: 14px;margin-top: -3px;}
    
    .article-header-meta-links.one-is-missing {padding-top: 8px!important;padding-bottom: 8px!important;}
    .article-header-meta .article-header-meta-links a {display: block;font-size: 12px;color: #3f484f;line-height: 150%;}
    .article-header-meta .article-header-meta-links a:hover {color: #e32e15;}
    .article-header-meta .article-header-meta-links a strong,
    .article-header-meta .article-header-meta-links a span {border-bottom: 1px dotted transparent;transition: border-bottom 0.2s;-webkit-transition: border-bottom 0.2s;-moz-transition: border-bottom 0.2s;}
    .article-header-meta .article-header-meta-links a:hover strong,
    .article-header-meta .article-header-meta-links a:hover span {border-bottom: 1px dotted #e32e15;}
    .article-header-meta .article-header-meta-links a i.fa {padding-right: 7px;}
    .social-headers {display: block;cursor: default;margin-top: -3px;margin-bottom: -3px;}
    .social-headers a {display: inline-block;height: 35px;width: 38px;text-align: center;line-height: 38px;background: transparent;color: #232323;border-bottom: 2px solid #232323;margin-left: 2px;font-size: 21px;}
    .social-headers a:hover {color: #fff!important;background: #232323;}
    .social-headers a.soc-whatsapp {color: #128c7e;border-bottom: 2px solid #128c7e;}
    .social-headers a.soc-whatsapp:hover {background: #128c7e;}
    .social-headers a.soc-facebook {color: #3b5998;border-bottom: 2px solid #3b5998;}
    .social-headers a.soc-facebook:hover {background: #3b5998;}
    .social-headers a.soc-twitter {color: #00aced;border-bottom: 2px solid #00aced;}
    .social-headers a.soc-twitter:hover {background: #00aced;}

    /*480PX BREAKPOINT*/
    @media(max-width: 30em){
        .right{float:left}
        .display{display:block}
        .article-header-info {padding-top: 10px;}
        .social-headers{margin-bottom:5px}
        .article-header-meta-links.one-is-missing{display:none!important}
    }
</style>
<main class="main_content container">
    <div class="content">

        <div class="main_content_left container">
            <article class="main_single_content" style="border-top-color:<?= $cor; ?>;">
                <header>
                    <h1><?= $post->channel; ?></h1>
                    <picture alt="<?= $post->channel; ?>">
                        <source media="(min-width: 540px)" srcset="<?=image($post->cover, 960, 490);?>"/>
                        <source media="(min-width: 1px)" srcset="<?=image($post->cover, 540, 260);?>"/>                   
                        <img src="<?=image($post->cover, 960, 490);?>" alt="<?= $post->channel; ?>" title="<?= $post->channel; ?>"/>
                    </picture>
                    
                    <div class="main_single_content_header_share">                            
                        <div class="article-header-info">
                            <div class="right social-headers">
                                <a target="_blank" href="https://api.whatsapp.com/send?text=Veja esta matéria: <?= url("/enquete/{$post->uri}"); ?> - É notícia? Tá no Portal SBN | Sistema Brasileiro de Notícias!" class="soc-whatsapp ot-share"><i class="fa fa-whatsapp"></i></a>
                                <a target="_blank" href="http://www.facebook.com/sharer/sharer.php?u=<?= url("/enquete/{$post->uri}"); ?>" data-url="<?= url("/enquete/{$post->uri}"); ?>" class="soc-facebook ot-share"><i class="fa fa-facebook"></i></a>
                                <a target="_blank" href="https://twitter.com/intent/tweet?original_referer=<?= url("/enquete/{$post->uri}"); ?>&amp;source=tweetbutton&amp;text=<?= $post->channel;?>&amp;url=<?= url("/enquete/{$post->uri}"); ?>&amp;via=" data-hashtags="" data-url="<?= url("/enquete/{$post->uri}"); ?>" data-via="" data-text="<?= $post->channel;?>" class="soc-twitter ot-tweet" target="_blank"><i class="fa fa-twitter"></i></a>
                            </div>
                            <div class="clear display"></div>
                            <span class="article-header-meta">
                            <?php
                            $mes = date('m', strtotime($post->created_at));
                            switch ($mes){

                                case 1: $mes = "janeiro"; break;
                                case 2: $mes = "fevereiro"; break;
                                case 3: $mes = "março"; break;
                                case 4: $mes = "abril"; break;
                                case 5: $mes = "maio"; break;
                                case 6: $mes = "junho"; break;
                                case 7: $mes = "julho"; break;
                                case 8: $mes = "agosto"; break;
                                case 9: $mes = "setembro"; break;
                                case 10: $mes = "outubro"; break;
                                case 11: $mes = "novembro"; break;
                                case 12: $mes = "dezembro"; break;
                                    
                                }
                            ?>
                                <span class="article-header-meta-date"><?= date('d', strtotime($post->created_at)); ?> <?= $mes;?></span>
                                <span class="article-header-meta-time">
                                    <span class="head-time"><?= date('H:i', strtotime($post->created_at)); ?></span>
                                    <span class="head-year"><?= date('Y', strtotime($post->created_at)); ?></span>
                                </span>
                                <span class="article-header-meta-links one-is-missing">
                                    <a href="#">
                                        <i class="fa fa-print"></i><span>Imprimir notícia</span>
                                    </a>
                                </span>
                            </span>
                        </div>
                        <div class="clear"></div>
                    </div>
                </header>
                <div class="clear"></div>

                <?php
                    if ($post->expire_at < date("Y-m-d H:i:s")){
                        //PEGA ENQUETE E COLOCA UM DISPLAY NONE NO TÍTULO
                        $v->insert("enquete", ["enquete" => $post, "display" => "display:none", "class_none2" => "display-none","class_none" => ""]);
                    } else {
                        //PEGA ENQUETE E COLOCA UM DISPLAY NONE NO TÍTULO
                        $v->insert("enquete", ["enquete" => $post, "display" => "display:none"]);
                    }
                ?>
                <!-- Composite Start -->
                <div id="M690472ScriptRootC1093276">
                </div>
                <script src="https://jsc.mgid.com/p/o/portalsbn.com.br.1093276.js" async></script>
                <!-- Composite End -->

                <div class="outbrain-tm" id="76640-16"><script src="//ads.themoneytizer.com/s/gen.js?type=16"></script><script src="//ads.themoneytizer.com/s/requestform.js?siteId=76640&formatId=16"></script></div>
            </article><!-- Content -->

            <div class="main_single_content_share_bottom">

                <div style="background:#fff;padding:5px 10px 15px 10px">
                    <div class="right social-headers">
                        <a target="_blank" href="https://api.whatsapp.com/send?text=Veja esta matéria: <?= url("/artigo/{$post->uri}"); ?> - É notícia? Tá no Portal SBN | Sistema Brasileiro de Notícias!" class="soc-whatsapp ot-share"><i class="fa fa-whatsapp"></i></a>
                        <a target="_blank" href="http://www.facebook.com/sharer/sharer.php?u=<?= url("/artigo/{$post->uri}"); ?>" data-url="<?= url("/artigo/{$post->uri}"); ?>" class="soc-facebook ot-share"><i class="fa fa-facebook"></i></a>
                        <a target="_blank" href="https://twitter.com/intent/tweet?original_referer=<?= url("/artigo/{$post->uri}"); ?>&amp;source=tweetbutton&amp;text=<?= $post->channel;?>&amp;url=<?= url("/artigo/{$post->uri}"); ?>&amp;via=" data-hashtags="" data-url="<?= url("/artigo/{$post->uri}"); ?>" data-via="" data-text="<?= $post->channel;?>" class="soc-twitter ot-tweet" target="_blank"><i class="fa fa-twitter"></i></a>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div><!-- Share Bottom -->

            <div class="main_banner" style="margin: 10px 0">
                <div id="76640-28"><script src="//ads.themoneytizer.com/s/gen.js?type=28"></script><script src="//ads.themoneytizer.com/s/requestform.js?siteId=76640&formatId=28"></script></div>
            </div>

            <aside class="main_banner margin-bottom-20 margin-top-10">
                <h1 class="font-zero">Publicidade</h1>
                <div class="banner_728 banner eureka_horizontal" style="width: 100%; max-width:728px;"><?= bannerAds(2,728,90); ?></div>
                <div class="banner_220 banner"><?= bannerAds(1,220,90); ?></div>
            </aside><!-- banner -->

            <article class="main_single_comments">
                <header>
                    <h1>Deixe seu comentário</h1>
                </header>
                <div class="fb-comments" width="100%" data-href="<?=url("/artigo/{$post->uri}");?>" data-numposts="5"></div>
            </article><!-- Comments -->

            <div class="main_sidebar_bottom container">
                <?php //require (REQUIRE_PATH . '/inc/sidebar_bottom_single.inc.php'); ?>    
            </div><!-- SIDEBAR BOTTOM -->

            <div class="main_sidebar_top container">
                <?php //require (REQUIRE_PATH . '/inc/sidebar_top_single.inc.php'); ?>
            </div><!-- SIDEBAR TOP -->
        </div><!-- CONTENT LEFT -->

        <div class="main_content_right container">
            <div class="main_sidebar container">
                <?php require (__DIR__ . '/inc/sidebar_top_single.php'); ?>
                <?php require (__DIR__ . '/inc/sidebar_bottom_single.php'); ?> 
            </div>
        </div><!-- CONTENT RIGHT -->

        <div class="clear"></div>
    </div>
</main>