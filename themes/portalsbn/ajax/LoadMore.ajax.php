<?php
require_once '../../../_app/Config.inc.php';
//RECEBE SUBMIT DO FORMULARIO VIA POST
$getData = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$setData = array_map("strip_tags", $getData);
$setData = array_map("trim", $setData);

$offset = $setData['offset'];
$estate = $setData['estate'];
$jSon['accept'] = null;
$readNews = new Read;
switch ($setData['link']):
    case 'index':
        $readNews->FullRead("SELECT pc_posts.*,pc_categories.* FROM pc_posts_categories "
                . "INNER JOIN pc_posts ON pc_posts.post_id = pc_posts_categories.post_id "
                . "INNER JOIN pc_categories ON pc_categories.category_id = pc_posts_categories.category_id "
                . "WHERE pc_posts.post_type = :type AND pc_posts.post_status = 1 AND pc_posts.post_trash = 0 AND pc_posts.post_estate LIKE '%$estate%' ORDER BY pc_posts.post_date DESC LIMIT :limit OFFSET :offset", "&type=post&limit=12&offset={$offset}");
        if ($readNews->getResult()):
            $pubdate = new PubDate;
            $jSon['offset'] = $offset;
            foreach ($readNews->getResult() as $MaisDestaques):
                $MaisDestaques['pubdate'] = $pubdate->ExePubDate($MaisDestaques['post_date']);
                $MaisDestaques['datetime'] = $MaisDestaques['post_date'];
                $MaisDestaques['post_title'] = Check::Words($MaisDestaques['post_title'], 8);
                $MaisDestaques['midia_icon'] = ( strpos($MaisDestaques['post_content'], "youtube.com/embed/") ? "<div style=\"top:-17px; bottom:auto;\" class=\"main_midia_icon\">Vídeo <i class=\"fa fa-youtube-play\"></i></div>" : (strpos($MaisDestaques['post_content'], "#galeria#") ? "<div style=\"top:-17px; bottom:auto;\" class=\"main_midia_icon\">Galeria <i class=\"fa fa-picture-o\"></i></div>" : "") );
                $MaisDestaques['facebook_app'] = FACEBOOK_APP;
                $jSon['accept'] .= "<article class=\"main_box_news main_box_white item\">
                                        <a href=\"" . HOME . "/" . SINGLEPOST . "/{$MaisDestaques['post_name']}\" title=\"{$MaisDestaques['post_title']}\">
                                            <picture title=\"{$MaisDestaques['post_title']}\">
                                                <source media=\"(min-width: 768px)\" srcset=\"" . HOME . "/tim.php?src=uploads/images/{$MaisDestaques['post_cover']}&w=520&h=320&q=90\"/>
                                                <source media=\"(min-width: 520px)\" srcset=\"" . HOME . "/tim.php?src=uploads/images/{$MaisDestaques['post_cover']}&w=520&h=344&q=90\"/>
                                                <source media=\"(min-width: 1px)\" srcset=\"" . HOME . "/tim.php?src=uploads/images/{$MaisDestaques['post_cover']}&w=480&h=240&q=90\"/>                   
                                                <img src=\"" . HOME . "/uploads/images/{$MaisDestaques['post_cover']}\" alt=\"{$MaisDestaques['post_title']}\" title=\"{$MaisDestaques['post_title']}\"/>
                                            </picture>
                                        </a>

                                        <div class=\"main_box_news_desc\">
                                            <ul class=\"social\">
                                                <li class=\"social_item\"><a href=\"http://www.facebook.com/sharer.php?u=" . HOME . "/" . SINGLEPOST . "/{$MaisDestaques['post_name']}&app_id={$MaisDestaques['facebook_app']}\" title=\"Compartilhe no Facebook\" onclick=\"window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;\" class=\"facebook\" rel=\"nofollow\"><i class=\"fa fa-facebook\"></i></a></li>
                                                <li class=\"social_item\"><a href=\"https://twitter.com/intent/tweet?url=" . HOME . "/" . SINGLEPOST . "/{$MaisDestaques['post_name']}&text={$MaisDestaques['post_title']}\" title=\"Conte isto no Twitter\" onclick=\"window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;\" class=\"twitter\" rel=\"nofollow\"><i class=\"fa fa-twitter\"></i></a></li>
                                                <li class=\"social_item\"><a href=\"http://plus.google.com/share?url=" . HOME . "/" . SINGLEPOST . "/{$MaisDestaques['post_name']}\" title=\"Compartilhe no Google+\" onclick=\"window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;\" class=\"google\" rel=\"nofollow\"><i class=\"fa fa-google-plus\"></i></a></li>
                                            </ul>

                                            <a href=\"" . HOME . "/" . SINGLEPOST . "/{$MaisDestaques['post_name']}\" title=\"{$MaisDestaques['post_title']}\">
                                                {$MaisDestaques['midia_icon']}
                                                <mark style=\"background-color:{$MaisDestaques['category_color']};\" class=\"categoria\">{$MaisDestaques['category_title']}</mark>
                                                <p class=\"tagline\">{$MaisDestaques['post_tag']}</p>
                                                <time datetime=\"{$MaisDestaques['datetime']}\">{$MaisDestaques['pubdate']}</time>
                                                <div class=\"clear\"></div>
                                                <h1>{$MaisDestaques['post_title']}</h1>
                                            </a>
                                        </div>
                                    </article>";
            endforeach;
        else:
            $jSon['accept'] = null;
        endif;
        break;

    case 'entretenimento':
        $readNews->FullRead("SELECT * FROM pc_posts WHERE post_type = :type AND post_status = 1 AND post_entertaiment = 1 AND post_trash = 0 AND post_estate LIKE '%$estate%' ORDER BY post_date DESC LIMIT :limit OFFSET :offset", "type=gallery&limit=8&offset={$offset}");
        if ($readNews->getResult()):
            $pubdate = new PubDate;
            $jSon['offset'] = $offset;
            foreach ($readNews->getResult() as $OutrasCoberturas):
                $OutrasCoberturas['pubdate'] = $pubdate->ExePubDate($OutrasCoberturas['post_date']);
                $OutrasCoberturas['datetime'] = $OutrasCoberturas['post_date'];
                $OutrasCoberturas['post_title'] = Check::Words($OutrasCoberturas['post_title'], 11);
                $OutrasCoberturas['midia_icon'] = "";
                $OutrasCoberturas['category_title'] = $OutrasCoberturas['post_adress'];
                $OutrasCoberturas['category_color'] = $category_color;
                $MaisDestaques['facebook_app'] = FACEBOOK_APP;
                $jSon['accept'] .= "<article class=\"main_box_news main_box_white item\">
                                        <a href=\"" . HOME . "/" . SINGLEGALLERY . "/{$MaisDestaques['post_name']}\" title=\"{$MaisDestaques['post_title']}\">
                                            <picture title=\"{$MaisDestaques['post_title']}\">
                                                <source media=\"(min-width: 768px)\" srcset=\"" . HOME . "/tim.php?src=uploads/images/{$MaisDestaques['post_cover']}&w=520&h=320&q=90\"/>
                                                <source media=\"(min-width: 520px)\" srcset=\"" . HOME . "/tim.php?src=uploads/images/{$MaisDestaques['post_cover']}&w=520&h=344&q=90\"/>
                                                <source media=\"(min-width: 1px)\" srcset=\"" . HOME . "/tim.php?src=uploads/images/{$MaisDestaques['post_cover']}&w=480&h=240&q=90\"/>                   
                                                <img src=\"" . HOME . "/uploads/images/{$MaisDestaques['post_cover']}\" alt=\"{$MaisDestaques['post_title']}\" title=\"{$MaisDestaques['post_title']}\"/>
                                            </picture>
                                        </a>

                                        <div class=\"main_box_news_desc\">
                                            <ul class=\"social\">
                                                <li class=\"social_item\"><a href=\"http://www.facebook.com/sharer.php?u=" . HOME . "/" . SINGLEGALLERY . "/{$MaisDestaques['post_name']}&app_id={$MaisDestaques['facebook_app']}\" title=\"Compartilhe no Facebook\" onclick=\"window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;\" class=\"facebook\" rel=\"nofollow\"><i class=\"fa fa-facebook\"></i></a></li>
                                                <li class=\"social_item\"><a href=\"https://twitter.com/intent/tweet?url=" . HOME . "/" . SINGLEGALLERY . "/{$MaisDestaques['post_name']}&text={$MaisDestaques['post_title']}\" title=\"Conte isto no Twitter\" onclick=\"window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;\" class=\"twitter\" rel=\"nofollow\"><i class=\"fa fa-twitter\"></i></a></li>
                                                <li class=\"social_item\"><a href=\"http://plus.google.com/share?url=" . HOME . "/" . SINGLEGALLERY . "/{$MaisDestaques['post_name']}\" title=\"Compartilhe no Google+\" onclick=\"window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;\" class=\"google\" rel=\"nofollow\"><i class=\"fa fa-google-plus\"></i></a></li>
                                            </ul>

                                            <a href=\"" . HOME . "/" . SINGLEGALLERY . "/{$MaisDestaques['post_name']}\" title=\"{$MaisDestaques['post_title']}\">
                                                {$MaisDestaques['midia_icon']}
                                                <mark style=\"background-color:{$MaisDestaques['category_color']};\" class=\"categoria\">{$MaisDestaques['category_title']}</mark>
                                                <p class=\"tagline\">{$MaisDestaques['post_tag']}</p>
                                                <time datetime=\"{$MaisDestaques['datetime']}\">{$MaisDestaques['pubdate']}</time>
                                                <div class=\"clear\"></div>
                                                <h1>{$MaisDestaques['post_title']}</h1>
                                            </a>
                                        </div>
                                    </article>";
            endforeach;
        else:
            $jSon['accept'] = null;
        endif;
        break;

    default :
        $readCat = new Read;
        if (isset($setData['local'])):
            $readCat->ExeRead("pc_categories", "WHERE category_name = :name", "name={$setData['local']}");
        else:
            $readCat->ExeRead("pc_categories", "WHERE category_name = :name", "name=educacao");
        endif;
        $where = $setData['link'];
        $parse = "category={$readCat->getResult()[0]['category_id']}&";
        $readNews = new Read;
        $readNews->FullRead("SELECT pc_posts.*,pc_categories.* FROM pc_posts_categories "
                . "INNER JOIN pc_posts ON pc_posts.post_id = pc_posts_categories.post_id "
                . "INNER JOIN pc_categories ON pc_categories.category_id = pc_posts_categories.category_id "
                . "WHERE {$where} pc_posts.post_type = :type AND pc_posts.post_status = 1 AND pc_posts.post_trash = 0 AND pc_posts.post_estate LIKE '%$estate%' ORDER BY pc_posts.post_date DESC LIMIT :limit OFFSET :offset", "{$parse}type=post&limit=12&offset={$offset}");
        if ($readNews->getResult()):
            $pubdate = new PubDate;
            $jSon['offset'] = $offset;
            foreach ($readNews->getResult() as $MaisDestaques):
                $MaisDestaques['pubdate'] = $pubdate->ExePubDate($MaisDestaques['post_date']);
                $MaisDestaques['datetime'] = $MaisDestaques['post_date'];
                $MaisDestaques['post_title'] = Check::Words($MaisDestaques['post_title'], 8);
                $MaisDestaques['midia_icon'] = ( strpos($MaisDestaques['post_content'], "youtube.com/embed/") ? "<div style=\"top:-17px; bottom:auto;\" class=\"main_midia_icon\">Vídeo <i class=\"fa fa-youtube-play\"></i></div>" : (strpos($MaisDestaques['post_content'], "#galeria#") ? "<div style=\"top:-17px; bottom:auto;\" class=\"main_midia_icon\">Galeria <i class=\"fa fa-picture-o\"></i></div>" : "") );
                $MaisDestaques['facebook_app'] = FACEBOOK_APP;
                $jSon['accept'] .= "<article class=\"main_box_news main_box_white item\">
                                        <a href=\"" . HOME . "/" . SINGLEPOST . "/{$MaisDestaques['post_name']}\" title=\"{$MaisDestaques['post_title']}\">
                                            <picture title=\"{$MaisDestaques['post_title']}\">
                                                <source media=\"(min-width: 768px)\" srcset=\"" . HOME . "/tim.php?src=uploads/images/{$MaisDestaques['post_cover']}&w=520&h=320&q=90\"/>
                                                <source media=\"(min-width: 520px)\" srcset=\"" . HOME . "/tim.php?src=uploads/images/{$MaisDestaques['post_cover']}&w=520&h=344&q=90\"/>
                                                <source media=\"(min-width: 1px)\" srcset=\"" . HOME . "/tim.php?src=uploads/images/{$MaisDestaques['post_cover']}&w=480&h=240&q=90\"/>                   
                                                <img src=\"" . HOME . "/uploads/images/{$MaisDestaques['post_cover']}\" alt=\"{$MaisDestaques['post_title']}\" title=\"{$MaisDestaques['post_title']}\"/>
                                            </picture>
                                        </a>

                                        <div class=\"main_box_news_desc\">
                                            <ul class=\"social\">
                                                <li class=\"social_item\"><a href=\"http://www.facebook.com/sharer.php?u=" . HOME . "/" . SINGLEPOST . "/{$MaisDestaques['post_name']}&app_id={$MaisDestaques['facebook_app']}\" title=\"Compartilhe no Facebook\" onclick=\"window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;\" class=\"facebook\" rel=\"nofollow\"><i class=\"fa fa-facebook\"></i></a></li>
                                                <li class=\"social_item\"><a href=\"https://twitter.com/intent/tweet?url=" . HOME . "/" . SINGLEPOST . "/{$MaisDestaques['post_name']}&text={$MaisDestaques['post_title']}\" title=\"Conte isto no Twitter\" onclick=\"window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;\" class=\"twitter\" rel=\"nofollow\"><i class=\"fa fa-twitter\"></i></a></li>
                                                <li class=\"social_item\"><a href=\"http://plus.google.com/share?url=" . HOME . "/" . SINGLEPOST . "/{$MaisDestaques['post_name']}\" title=\"Compartilhe no Google+\" onclick=\"window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;\" class=\"google\" rel=\"nofollow\"><i class=\"fa fa-google-plus\"></i></a></li>
                                            </ul>

                                            <a href=\"" . HOME . "/" . SINGLEPOST . "/{$MaisDestaques['post_name']}\" title=\"{$MaisDestaques['post_title']}\">
                                                {$MaisDestaques['midia_icon']}
                                                <mark style=\"background-color:{$MaisDestaques['category_color']};\" class=\"categoria\">{$MaisDestaques['category_title']}</mark>
                                                <p class=\"tagline\">{$MaisDestaques['post_tag']}</p>
                                                <time datetime=\"{$MaisDestaques['datetime']}\">{$MaisDestaques['pubdate']}</time>
                                                <div class=\"clear\"></div>
                                                <h1>{$MaisDestaques['post_title']}</h1>
                                            </a>
                                        </div>
                                    </article>";
            endforeach;
        else:
            $jSon['accept'] = null;
        endif;
        break;
endswitch;

echo json_encode($jSon);