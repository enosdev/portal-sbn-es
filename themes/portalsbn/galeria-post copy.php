<?php $v->layout("_theme"); ?>

<main class="main_content container">
    <div class="content">

        <div class="main_content_left container" style="width:100%; margin-bottom: 20px;">
            <article class="main_single_content" style="border-top-color:<?= $cor; ?>;">
                <header>
                    <h1><?=$post->title;?></h1>
                    <div class="main_single_content_header_excerpt">
                        <p class="tagline"><?=date("d/m/Y", strtotime($post->event_date));?> - <?=$post->local;?> | <?=$count;?> imagens | Esse evento foi visto <?=$post->views;?> vezes</p>
                    </div>
                </header>
                <div class="clear"></div>
                <ul>
                    <?php
                    if($image[0] != 'no_image'):
                            foreach ($image as $img):
                                $photo = explode('storage',$img);
                                $v->insert("galeria-photo", ["img" => end($photo)]);
                            endforeach;
                        else:?>
                            <div class="htmlchars align-center">
                                <p>Pasta de imagens da galeria <strong><?=$post->title;?></strong> ainda vazia.</p>
                            </div>
                        <?php endif;?>
                </ul>
                
            </article><!-- Content -->
            <?=$paginator;?>
        </div><!-- CONTENT FULL -->
        <div class="clear"></div>
    </div>
</main>