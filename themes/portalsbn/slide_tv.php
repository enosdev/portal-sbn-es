<?php

if($mod == 'mobile'):
    $strVideo = strpos($video->content, "youtube.com/embed/");
    $idVideo = substr($video->content, $strVideo + 18, 11);
?>

<article class="main_box_news main_box_gray slide_mobile_item margin-top-0 slide_mobile_item_videos<?= $first; ?>">
    <a data-fancybox="" data-type="iframe" href="https://www.youtube.com/embed/<?= $idVideo; ?>?ecver=2"><i class="fa fa-play round"></i></a>
    <a data-fancybox="" data-type="iframe" href="https://www.youtube.com/embed/<?= $idVideo; ?>?ecver=2">
        <img class="img-fluid lazyload" data-src="<?= image($video->cover, 520, 310); ?>" alt="<?=$video->title;?>" title="<?=$video->title;?>"/>
    </a>
    <div class="main_box_news_desc">
    <a data-fancybox="" href="<?=$video->video;?>">
            <mark style="background-color:<?=$video->category()->color;?>; " class="categoria"><?=$video->category()->title;?></mark>
            <p class="tagline"><?=$video->tag;?></p>
            <time datetime="<?= date('Y-m-d H:i:s', strtotime($video->post_at));?>"><?= date('d/m/Y', strtotime($video->post_at));?></time>
            <div class="clear"></div>
            <h1><?= str_limit_chars($video->title, 80); ?></h1>
        </a>
    </div>
</article><!-- Item mobile -->

<?php 
elseif($mod == 'normal'):
    $strVideo = strpos($video->content, "youtube.com/embed/");
    $idVideo = substr($video->content, $strVideo + 18, 11);
?>

<div class="main_slide_videos_item slide_item_videos<?= $first; ?>">
    <a data-fancybox="" data-type="iframe" href="https://www.youtube.com/embed/<?= $idVideo; ?>?ecver=2"><i class="fa fa-play round"></i></a>
    <a data-fancybox="" data-type="iframe" href="https://www.youtube.com/embed/<?= $idVideo; ?>?ecver=2">
        <img class="img-fluid lazyload" data-src="<?= image($video->cover, 840, 605); ?>" alt="<?=$video->title;?>" title="<?=$video->title;?>"/>
    </a>
    <div class="main_slide_videos_item_desc">
        <a data-fancybox="" data-type="iframe" href="https://www.youtube.com/embed/<?= $idVideo; ?>?ecver=2">
            <mark style="background-color:<?=$video->category()->color;?>; " class="categoria"><?=$video->category()->title;?></mark>
            <p class="tagline"><?=$video->tag;?></p>
            <time datetime="<?= date('Y-m-d H:i:s', strtotime($video->post_at));?>"><?= date('d/m/Y', strtotime($video->post_at));?></time>
            <div class="clear"></div>
            <p class="title"><?=$video->title;?></p>
        </a>
    </div>
</div>

<?php
elseif($mod == 'link'):?>

<div class="main_slide_videos_item_click slide_mark_videos<?= $active; ?>" id="<?= ($i - 1); ?>">
    <mark style="background-color:<?= $video->category()->color; ?>; " class="categoria"><?= $video->category()->title; ?></mark>
    <p class="tagline"><?= $video->tag; ?></p>
    <div class="clear"></div>
    <p class="title"><?=str_limit_chars($video->title, 80);?></p>
    <div style="bottom:0;" class="main_midia_icon">Vídeo <i class="fa fa-youtube-play"></i></div>
</div><!-- Item click -->

<?php
endif;