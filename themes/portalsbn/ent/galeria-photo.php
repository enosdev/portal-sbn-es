<li>
    <a data-fancybox="fotos" data-caption="<?= $post->title; ?> - <?= $post->local; ?>"
        href="<?= url("/".CONF_UPLOAD_DIR."{$img}"); ?>"
        title="<?= $post->title; ?> - <?= $post->local; ?>">
        <img src="<?= image($img, 600); ?>"
            alt="<?= $post->title; ?> - <?= $post->local; ?>" title="<?= $post->title; ?> - <?= $post->local; ?>">
    </a>
</li>