<?php $v->layout("ent/_theme-ent-gal"); ?>

<div class="info">
    <h1><?=$post->title;?></h1>
    <p><?=date("d/m/Y", strtotime($post->event_date));?>, <?=$post->local;?> | <?=$count;?> imagens</p>
    <div class="bg"
        style="background-image: url(<?=theme("/img/banner-gallery.jpg");?>);">
    </div>
</div>

<div class="gallery">
    <ul>
        <?php
        if($image[0] != 'no_image'):
            foreach ($image as $img):
                $photo = explode(CONF_UPLOAD_DIR,$img);
                $v->insert("ent/galeria-photo", ["img" => end($photo)]);
            endforeach;
        else:?>
            <div style="margin:20px; text-align:center;color:#fff;">
                <p>Pasta de imagens da galeria <strong><?=$post->title;?></strong> ainda vazia.</p>
            </div>
        <?php endif;?>
    </ul>
</div>