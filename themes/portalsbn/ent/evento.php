<?php $v->layout('ent/_theme-ent');?>

<div class="box-left">
    <div class="banner">
        <?= bannerAds(1,728,90); ?>
    </div>

    <div class="category-title" style="border-bottom:none">
        <!-- notícias relacionadas a entretenimento -->
        <h1 class="mobile"><?= $post->title; ?></h1>
        <p class="tagline" style="font-size:.8em;">Publicado: <?= date('d/h/Y', strtotime($post->post_at)); ?> | <?= $post->views; ?> vizualisações</p>
    </div>
    
    <div class="htmlchars" style="word-wrap: break-word; margin:20px 0">
        <?= (!is_null($post->cover) ? "<img style=\"margin: 0 20%\" width=\"60%\" src=".url("/storage/$post->cover")." title=".$post->title." alt=".$post->title."/>" : "") ;?>
        <br>
        <?= $post->details;?>
    </div>
    
</div>

<div class="box-right">
    <?php require(__DIR__."/aside.php"); ?>
</div>


