
<?php
    //função para dexar o menu selecionado
    if(isset($idd)){
        $mm = (new Source\Models\Category())
    ->find('id = :p', "p={$idd}", "parent")
    ->fetch()->parent;
    }
    $categoryId = ($mm ?? '')? $mm : $idd ?? '';
    $activeHome = function ($value) use ($categoryId) {
        return ($categoryId == $value ? "active" : "");
    };

    $url = ($menu->uri == 'entretenimento')? url("/{$menu->uri}") : url("/artigo/lista/{$menu->uri}") ;
?>

<li class="main_header_nav_desktop_item <?=$activeHome($menu->id);?>"><a href="<?=$url;?>"><?=$menu->title;?></a>
    <ul class="main_header_nav_desktop_sub">
        <div class="content">
            <?php
            $menuCat = (new Source\Models\Category())->find("type = :t && parent = :id","t=post&id={$menu->id}")->fetch(true);
            $contaSub = ($menuCat)? 3 : 4;
            $header = (new Source\Models\Post())
                ->findPost('type = :t && category = :c', "t=post&c={$menu->id}")
                ->order("post_at DESC")
                ->limit($contaSub)->fetch(true);
            foreach($header as $nm):
               $icon = ( strpos($nm->content, "youtube.com/embed/") ? "<div style=\"top:-17px; bottom:auto;\" class=\"main_midia_icon\">Vídeo <i class=\"fa fa-youtube-play\"></i></div>" : (strpos($nm->content, "@#[") ? "<div style=\"top:-17px; bottom:auto;\" class=\"main_midia_icon\">Galeria <i class=\"fa fa-picture-o\"></i></div>" : "") );
            ?>
                <article class="main_box_news main_box_white item">
                    <a href="<?= url("/artigo/{$nm->uri}");?>" title="<?=$nm->title;?>">
                        <img class="lazyload" data-src="<?= image($nm->cover, 480,240); ?>" alt="<?=$nm->title;?>" title="<?=$nm->title;?>"/>
                    </a>
                    <div class="main_box_news_desc" style="min-height: auto;">
                        <ul class="social">
                            <li class="social_item"><a href="https://api.whatsapp.com/send?text=Veja esta matéria:<?= url("/artigo/{$nm->uri}");?>" title="Compartilhe WhatsApp" onclick="window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;" class="whatsapp" rel="nofollow"><i class="fa fa-whatsapp"></i></a></li>
                            <li class="social_item"><a href="http://www.facebook.com/sharer.php?u=<?= url("/artigo/{$nm->uri}");?>" title="Compartilhe no Facebook" onclick="window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;" class="facebook" rel="nofollow"><i class="fa fa-facebook"></i></a></li>
                            <li class="social_item"><a href="https://twitter.com/intent/tweet?url=<?= url("/artigo/{$nm->uri}");?>&text=<?=$nm->title;?>" title="Conte isto no Twitter" onclick="window.open(this.href, '_blank', 'scrollbars=yes, status=no, toolbar=no, location=no, directories=no, menubar=no, resizable=no, fullscreen=no, width=500, height=600, top=100, left=200'); return false;" class="twitter" rel="nofollow"><i class="fa fa-twitter"></i></a></li>
                        </ul><!-- social share -->

                        <a href="<?= url("/artigo/{$nm->uri}");?>" title="<?=$nm->title;?>">
                            <?=$icon;?>
                            <mark style="background-color:<?= $nm->category()->color;?>" class="categoria"><?= $nm->category()->title;?></mark>
                            <p class="tagline" style="color:#666;"><?=$nm->tag;?></p>
                            <time datetime="<?= date('Y-m-d H:i:s', strtotime($nm->post_at));?>" style="margin-top: 4px;"><?= date('d/m/Y', strtotime($nm->post_at));?></time>
                            <div class="clear"></div>

                            <h1><?=str_limit_chars($nm->title, 70);?></h1>
                        </a>
                    </div>
                </article>
            <?php endforeach;

            /* Exibe menu de sub-categorias se existir */
            if ($menuCat): ?>
                <div class="main_header_nav_desktop_sub_list">
                    <p class="title">Sub-categorias</p>
                    <?php foreach ($menuCat as $Cat):
                        ?>
                        <li class="main_header_nav_desktop_sub_item"><a href="<?=url("/artigo/em/{$Cat->uri}");?>" title=""><?= $Cat->title; ?></a></li>
                        <?php
                    endforeach;?>
               </div>
            <?php endif;?>
        </div>
    </ul>
</li>