// JQUERY INIT

$(function () {
	//CARREGA O ENDEREÇO DO SITE
	BASE = $("link[rel='canonical']").attr('href');

	// menu mobile
	$('.menu').on('click', function () {
		$('.overlay').fadeToggle('fast', function () {
			$('.menu-mobile').css('right', 0);
		});
	});
	$('.overlay').on('click', function () {
		$('.overlay').fadeToggle('fast', function () {
			$('.menu-mobile').css('right', -220);
		});
	});

	//copia código da galeria selecionada
	$('.copy').click(function () {
		$('.allowCopy').focus();
		$('.allowCopy').select();
		var copy = document.execCommand('copy');
		if (copy) {
			$('.alertando').text('"GALERIA" Copiada com sucesso, agora cole no texto!!!').fadeIn('slow');
			$('.alertando').delay(8500).fadeOut('slow');
		} else {
			$('.alertando').text('Erro ao copiar tente novamente').fadeIn('fast');
			$('.alertando').delay(2500).fadeOut('fast');
		}
	});

	//copia código dos links veja mais
	$('.copylink').click(function () {
		$('.CopyVeja').focus();
		$('.CopyVeja').select();
		var copylink = document.execCommand('copy');
		if (copylink) {
			$('.alertando-links').text('"VEJA MAIS" copiado com sucesso, agora cole no texto!!!').fadeIn('slow');
			$('.alertando-links').delay(8500).fadeOut('slow');
		} else {
			$('.alertando-links').text('Erro ao copiar tente novamente').fadeIn('fast');
			$('.alertando-links').delay(2500).fadeOut('fast');
		}
	});

	//ao selecionar a galeria mostra o código pra copiar
	$('select#gallery').change(function () {
		var code = $(this).find(':selected').val();
		if (code == '') {
			$('.allowCopy').val('');
		} else {
			$('.allowCopy').val('[[' + code + ']]');
		}
	});

	//selecionar cores na categoria e subcategoria
	$('select[name="parent"]').change(function () {
		var color = $(this).find(':selected').attr('data-color');
		//$('input[name="color"]').val(color);
		if (color == undefined) {
			$('input[name="color"]').val('#707070');
			$('input[type="file"]').removeAttr('disabled');
		} else {
			$('input[name="color"]').val(color);
			$('input[type="file"]').attr('disabled', 'disabled');
		}
	});

	//date time picker
	$.datetimepicker.setLocale('pt');
	$('.datetimepicker').datetimepicker({
		mask: '39/19/9999 29:59',
		format: 'd/m/Y H:i'
	});

	var ajaxResponseBaseTime = 3;
	var ajaxResponseRequestError =
		"<div class='message error fa fa-exclamation-triangle'> Desculpe mas não foi possível processar sua requisição...</div>";

	// MOBILE MENU

	$('.mobile_menu').click(function (e) {
		e.preventDefault();

		var menu = $('.dash_sidebar');
		menu.animate({ right: 0 }, 200, function (e) {
			$('body').css('overflow', 'hidden');
		});

		menu.one('mouseleave', function () {
			$(this).animate({ right: '-260' }, 200, function (e) {
				$('body').css('overflow', 'auto');
			});
		});
	});

	//NOTIFICATION CENTER

	function notificationsCount() {
		var center = $('.notification_center_open');
		$.post(
			center.data('count'),
			function (response) {
				if (response.count) {
					center.html(response.count);
				} else {
					center.html('0');
				}
			},
			'json'
		);
	}

	function notificationHtml(link, image, notify, date) {
		return (
			'<div data-notificationlink="' +
			link +
			'" class="notification_center_item radius transition">\n' +
			'    <div class="image">\n' +
			'        <img class="rounded" src="' +
			image +
			'"/>\n' +
			'    </div>\n' +
			'    <div class="info">\n' +
			'        <p class="title">' +
			notify +
			'</p>\n' +
			'        <p class="time icon-clock-o">' +
			date +
			'</p>\n' +
			'    </div>\n' +
			'</div>'
		);
	}

	notificationsCount();

	setInterval(function () {
		notificationsCount();
	}, 1000 * 50);

	$('.notification_center_open').click(function (e) {
		e.preventDefault();

		var notify = $(this).data('notify');
		var center = $('.notification_center');

		$.post(
			notify,
			function (response) {
				if (response.message) {
					ajaxMessage(response.message, ajaxResponseBaseTime);
				}

				var centerHtml = '';
				if (response.notifications) {
					$.each(response.notifications, function (e, notify) {
						centerHtml += notificationHtml(notify.link, notify.image, notify.title, notify.created_at);
					});

					center.html(centerHtml);

					center.css('display', 'block').animate({ right: 0 }, 200, function (e) {
						$('body').css('overflow', 'hidden');
					});
				}
			},
			'json'
		);

		center.one('mouseleave', function () {
			$(this).animate({ right: '-320' }, 200, function (e) {
				$('body').css('overflow', 'auto');
				$(this).css('display', 'none');
			});
		});

		notificationsCount();
	});

	$('.notification_center').on('click', '[data-notificationlink]', function () {
		window.location.href = $(this).data('notificationlink');
	});

	//DATA SET

	$('[data-post]').click(function (e) {
		e.preventDefault();

		var clicked = $(this);
		var data = clicked.data();
		var load = $('.ajax_load');

		if (data.confirm) {
			var deleteConfirm = confirm(data.confirm);
			if (!deleteConfirm) {
				return;
			}
		}

		$.ajax({
			url: data.post,
			type: 'POST',
			data: data,
			dataType: 'json',
			beforeSend: function () {
				load.fadeIn(200).css('display', 'flex');
			},
			success: function (response) {
				//redirect
				if (response.redirect) {
					window.location.href = response.redirect;
				} else {
					load.fadeOut(200);
				}

				//reload
				if (response.reload) {
					window.location.reload();
				} else {
					load.fadeOut(200);
				}

				//message
				if (response.message) {
					ajaxMessage(response.message, ajaxResponseBaseTime);
				}
			},
			error: function () {
				ajaxMessage(ajaxResponseRequestError, 5);
				load.fadeOut();
			}
		});
	});

	//FORMS
	$('#form_filter').change(function (e) {
		e.preventDefault();

		var form = $(this);
		var load = $('.ajax_load');

		if (typeof tinyMCE !== 'undefined') {
			tinyMCE.triggerSave();
		}

		form.ajaxSubmit({
			url: form.attr('action'),
			type: 'POST',
			dataType: 'json',
			beforeSend: function () {
				load.fadeIn(200).css('display', 'flex');
			},
			uploadProgress: function (event, position, total, completed) {
				var loaded = completed;
				var load_title = $('.ajax_load_box_title');
				load_title.text('Enviando (' + loaded + '%)');

				if (completed >= 100) {
					load_title.text('Aguarde, carregando...');
				}
			},
			success: function (response) {
				//redirect
				if (response.redirect) {
					window.location.href = response.redirect;
				} else {
					form.find("input[type='file']").val(null);
					load.fadeOut(200);
				}

				//reload
				if (response.reload) {
					window.location.reload();
				} else {
					load.fadeOut(200);
				}

				//message
				if (response.message) {
					ajaxMessage(response.message, ajaxResponseBaseTime);
				}
			},
			complete: function () {
				if (form.data('reset') === true) {
					form.trigger('reset');
				}
			},
			error: function () {
				ajaxMessage(ajaxResponseRequestError, 5);
				load.fadeOut();
			}
		});
	});

	$("form:not('.ajax_off')").submit(function (e) {
		e.preventDefault();

		var form = $(this);
		var load = $('.ajax_load');

		if (typeof tinyMCE !== 'undefined') {
			tinyMCE.triggerSave();
		}

		form.ajaxSubmit({
			url: form.attr('action'),
			type: 'POST',
			dataType: 'json',
			beforeSend: function () {
				load.fadeIn(200).css('display', 'flex');
			},
			uploadProgress: function (event, position, total, completed) {
				var loaded = completed;
				var load_title = $('.ajax_load_box_title');
				load_title.text('Enviando (' + loaded + '%)');

				if (completed >= 100) {
					load_title.text('Aguarde, carregando...');
				}
			},
			success: function (response) {
				//redirect
				if (response.redirect) {
					window.location.href = response.redirect;
				} else {
					form.find("input[type='file']").val(null);
					load.fadeOut(200);
				}

				//reload
				if (response.reload) {
					window.location.reload();
				} else {
					load.fadeOut(200);
				}

				//message
				if (response.message) {
					ajaxMessage(response.message, ajaxResponseBaseTime);
				}

				//image by fsphp mce upload
				if (response.mce_image) {
					$('.mce_upload').fadeOut(200);
					tinyMCE.activeEditor.insertContent(response.mce_image);
				}

				//file by fsphp mce upload
				if (response.mce_file) {
					$('.mce_upload2').fadeOut(200);
					tinyMCE.activeEditor.insertContent(response.mce_file);
				}
			},
			complete: function () {
				if (form.data('reset') === true) {
					form.trigger('reset');
				}
			},
			error: function () {
				ajaxMessage(ajaxResponseRequestError, 5);
				load.fadeOut();
			}
		});
	});

	// AJAX RESPONSE

	function ajaxMessage(message, time) {
		var ajaxMessage = $(message);

		ajaxMessage.append("<div class='message_time'></div>");
		ajaxMessage.find('.message_time').animate({ width: '100%' }, time * 1000, function () {
			$(this).parents('.message').fadeOut(200);
		});

		$('.ajax_response').append(ajaxMessage);
		ajaxMessage.effect('bounce');
	}

	// AJAX RESPONSE MONITOR

	$('.ajax_response .message').each(function (e, m) {
		ajaxMessage(m, (ajaxResponseBaseTime += 1));
	});

	// AJAX MESSAGE CLOSE ON CLICK

	$('.ajax_response').on('click', '.message', function (e) {
		$(this).effect('bounce').fadeOut(1);
	});

	// MAKS

	$('.mask-hour').mask('00:00');
	$('.mask-date').mask('00/00/0000');
	$('.mask-datetime').mask('00/00/0000 00:00');
	$('.mask-month').mask('00/0000', { reverse: true });
	$('.mask-doc').mask('000.000.000-00', { reverse: true });
	$('.mask-card').mask('0000  0000  0000  0000', { reverse: true });
	$('.mask-money').mask('000.000.000.000.000,00', { reverse: true, placeholder: '0,00' });

	// CARREGA DROPZONE PARA GALERIA
	if (window.location.href.indexOf('/gallery') !== -1) {
		//BASE = $("link[rel='canonical']").attr('href');

		$.getScript(BASE + '/../shared/scripts/dropzone.js', function () {
			$('head').append("<link rel='stylesheet' href='" + BASE + "/../shared/styles/dropzone.css'/>");
			const pasta = $('.dropzone input').val();
			$('#dropzone').dropzone({
				dictDefaultMessage:
					"<i class='fa fa-cloud-upload-alt fa-2x' style='color: #20a8d8';></i><br><span style='color:slategray;font-size: 1em'>Solte as imagens aqui para enviar ( Ou Click )</span>",
				url: BASE + '/../source/Support/Dropzone.php?name=' + pasta,
				resizeWidth: 800,
				// uploadMultiple: true,
				maxFilesize: 1,
				acceptedFiles: '.jpeg,.jpg,.png,.gif',
				// parallelUploads: 500,
				autoProcessQueue: true,
				addRemoveLinks: false
			});
			$('#submit-all').on('click', function () {
				$('#dropzone').get(0).dropzone.processQueue();
			});
		});
	}

	//FAZER APARECER A IMAGEM DE PRE-CARREGAMENTO ANTES DO UPLOAD
	$('#file').on('change', function () {
		if (typeof FileReader != 'undefined') {
			const image_holder = $('#image-holder');
			image_holder.empty();

			const reader = new FileReader();
			reader.onload = function (e) {
				$('<img />', {
					src: e.target.result,
					class: 'radius',
					style: 'width: 100%; margin-top: 30px'
				}).appendTo(image_holder);
			};
			image_holder.show();
			reader.readAsDataURL($(this)[0].files[0]);
		} else {
			alert('Este navegador nao suporta FileReader.');
		}
	});

	//funções da busca dinâmica
	// Multiple select

	//seleciona a categoria
	var valor_category = $('select[name="category"] option:selected').val()
	if (!valor_category) {
		valor_category = 0
	}
	$('select[name="category"]').change(function () {
		valor_category = $(this).val();
	});

	$('#multi_autocomplete').autocomplete({
		source: function (request, response) {
			var searchText = extractLast(request.term);
			if (searchText.length <= 4 && valor_category > 0) {
				$.ajax({
					url: BASE + '/blog/morenewscategory',
					type: 'post',
					dataType: 'json',
					data: {
						category: valor_category
					},
					success: function (data) {
						response(data);
					}
				});
			} else {
				$.ajax({
					url: BASE + '/blog/morenews',
					type: 'post',
					dataType: 'json',
					data: {
						search: searchText
					},
					success: function (data) {
						response(data);
					}
				});
			}
		},
		select: function (event, ui) {
			// var terms = split($('#multi_autocomplete').val());

			// terms.pop();

			// terms.push(ui.item.label);

			// terms.push('');
			// $('#multi_autocomplete').val(terms.join(', '));
			$('#multi_autocomplete').val('');
			$('#view_links').show();
			$('#view_links').append('<p>' + ui.item.label + '</p>');

			// Id
			var terms = split($('#selectuser_ids').val());

			terms.pop();

			terms.push(ui.item.value);

			terms.push('');
			$('#selectuser_ids').val(terms.join(', '));

			return false;
		}
	});

	if ($('#selectuser_ids').val() == '') {
		$('#view_links').hide();
	}

	//limpar seleção de busca
	$('.limpar-selecao').on('click', function () {
		$('input[name="seemore"]').val('');
		$('#view_links').html('<h2>Limpo com Sucesso</h2>');
	});
});
function split(val) {
	return val.split(/,\s*/);
}
function extractLast(term) {
	return split(term).pop();
}

tinymce.init({
	selector: 'textarea.mce',
	language: 'pt_BR',
	plugins:
		'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
	imagetools_cors_hosts: ['picsum.photos'],
	menubar: 'file edit view insert format tools table help',
	toolbar:
		'undo redo | bold italic underline strikethrough | formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap | fullscreen  preview print | insertfile image media link | ltr rtl | esdimg esdfile',
	mobile: {
		theme: 'mobile',
		plugins: ['paste', 'lists', 'autolink', 'image', 'link', 'media', 'imagetools', ''],
		toolbar: ['undo', 'bold', 'italic', 'styleselect', 'forecolor', 'esdimg', 'esdfile', 'image', 'media', 'link']
	},
	toolbar_sticky: true,
	image_advtab: true,
	image_class_list: [{ title: 'None', value: '' }, { title: 'Some class', value: 'class-name' }],
	importcss_append: true,
	template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
	template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
	height: 600,
	image_caption: true,
	relative_urls: false,
	convert_urls: true,
	quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
	noneditable_noneditable_class: 'mceNonEditable',
	toolbar_drawer: 'sliding',
	contextmenu: 'link image imagetools table',
	setup: function (editor) {
		editor.ui.registry.addButton('esdimg', {
			text: 'Enviar Imagem',
			icon: 'image',
			onAction: function (_) {
				$('.mce_upload')
					.fadeIn(200, function (e) {
						$('body').click(function (e) {
							if ($(e.target).attr('class') === 'mce_upload') {
								$('.mce_upload').fadeOut(200);
							}
						});
					})
					.css('display', 'flex');
			}
		});
		editor.ui.registry.addButton('esdfile', {
			text: 'Enviar Arquivo',
			icon: 'upload',
			onAction: function (_) {
				$('.mce_upload2')
					.fadeIn(200, function (e) {
						$('body').click(function (e) {
							if ($(e.target).attr('class') === 'mce_upload2') {
								$('.mce_upload2').fadeOut(200);
							}
						});
					})
					.css('display', 'flex');
			}
		});
	}
});

// TINYMCE INIT
// var nanospell_directory =
// 	location.href.substring(0, location.href.lastIndexOf('/admin') + 1) + 'shared/scripts/tinymce/plugins/nanospell/';
// // var nanospell_directory = 'http://localhost/sbn/shared/scripts/tinymce/plugins/nanospell/';
// tinyMCE.init({
// 	selector: 'textarea.mce',
// 	language: 'pt_BR',
// 	menubar: false,
// 	theme: 'modern',
// 	height: 400,
// 	skin: 'lightgray',
// 	entity_encoding: 'raw',
// 	theme_advanced_resizing: true,
// 	// plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount tinymcespellchecker a11ychecker imagetools textpattern help formatpainter permanentpen pageembed tinycomments mentions linkchecker',
// 	// toolbar: 'formatselect | bold italic strikethrough forecolor backcolor permanentpen formatpainter | link image media pageembed | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat | addcomment',
// 	image_advtab: true,

// 	plugins: [
// 		'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
// 		'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
// 		'save table contextmenu directionality emoticons template paste textcolor media nanospell'
// 	],
// 	toolbar:
// 		'styleselect | nanospell | pastetext | removeformat |  bold | italic | underline | strikethrough | forecolor | backcolor | bullist | numlist | alignleft | aligncenter | alignright | alignjustify | link | unlink | fsphpimage | fsphpfile | media | image | code | fullscreen',
// 	// 'English=en,Danish=da,Dutch=nl,Finnish=fi,French=fr_FR,' +
// 	// 'German=de,Italian=it,Polish=pl,Portuguese=pt_BR,Spanish=es,Swedish=sv',
// 	spellchecker_languages: 'Portuguese=pt_BR',

// 	spellchecker_callback: function(method, text, success, failure) {
// 		var words = text.match(this.getWordCharPattern());
// 		if (method === 'spellcheck') {
// 			var suggestions = {};
// 			for (var i = 0; i < words.length; i++) {
// 				suggestions[words[i]] = [ 'First', 'Second' ];
// 			}
// 			success({ words: suggestions, dictionary: [] });
// 		} else if (method === 'addToDictionary') {
// 			// Add word to dictionary here
// 			success();
// 		}
// 	},
// 	style_formats: [
// 		{ title: 'Normal', block: 'p' },
// 		{ title: 'Titulo 3', block: 'h3' },
// 		{ title: 'Titulo 4', block: 'h4' },
// 		{ title: 'Titulo 5', block: 'h5' },
// 		{ title: 'Código', block: 'pre', classes: 'brush: php;' }
// 	],
// 	link_class_list: [
// 		{ title: 'None', value: '' },
// 		{ title: 'Blue CTA', value: 'btn btn_cta_blue' },
// 		{ title: 'Green CTA', value: 'btn btn_cta_green' },
// 		{ title: 'Yellow CTA', value: 'btn btn_cta_yellow' },
// 		{ title: 'Red CTA', value: 'btn btn_cta_red' }
// 	],
// setup: function(editor) {
// 	editor.addButton('fsphpimage', {
// 		title: 'Enviar Imagem',
// 		icon: 'image',
// 		onclick: function() {
// $('.mce_upload')
// 	.fadeIn(200, function(e) {
// 		$('body').click(function(e) {
// 			if ($(e.target).attr('class') === 'mce_upload') {
// 				$('.mce_upload').fadeOut(200);
// 			}
// 		});
// 	})
// 	.css('display', 'flex');
// 		}
// 	});
// 		editor.addButton('fsphpfile', {
// 			title: 'Enviar Arquivo',
// 			icon: 'upload',
// 			onclick: function() {
// 				$('.mce_upload2')
// 					.fadeIn(200, function(e) {
// 						$('body').click(function(e) {
// 							if ($(e.target).attr('class') === 'mce_upload2') {
// 								$('.mce_upload2').fadeOut(200);
// 							}
// 						});
// 					})
// 					.css('display', 'flex');
// 			}
// 		});
// 	},
// 	link_title: false,
// 	target_list: false,
// 	theme_advanced_blockformats: 'h1,h2,h3,h4,h5,p,pre',
// 	media_dimensions: false,
// 	media_poster: false,
// 	media_alt_source: false,
// 	media_embed: true,
// 	extended_valid_elements: 'a[href|target=_blank|rel|class|style]',
// 	imagemanager_insert_template: '<img src="{$url}" title="{$title}" alt="" />',
// 	image_dimensions: false,
// 	relative_urls: false,
// 	remove_script_host: false,
// 	paste_as_text: true,
// 	external_plugins: { nanospell: nanospell_directory + 'plugin.js' },
// 	nanospell_server: 'php', // choose "php" "asp" "asp.net" or "java"
// 	nanospell_autostart: true,
// 	nanospell_dictionary: 'pt_br'
// });
