<div class="main_sidebar">
    <h3><i class="fas fa-laptop"></i> dash/publicidades</h3>
    <p class="dash_content_sidebar_desc">Gerencie, monitore e acompanhe as publicidades do seu site aqui...</p>

    <?php if (!empty($publicity) && $publicity->photo()): ?>
        <div id="image-holder">
            <img class="radius" style="width: 100%; margin-top: 30px" src="<?= image($publicity->photo, 300); ?>"/>
        </div>
    <?php else: ?>
        <div id="image-holder"></div>
    <?php endif;?>
</div>