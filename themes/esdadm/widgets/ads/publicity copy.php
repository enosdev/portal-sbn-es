<?php $v->layout("_admin"); ?>
<div class="desc"><i class="far fa-images"></i> Publicidades</div>


<main>
<?php $v->insert("widgets/ads/sidebar.php"); ?>
    <?php if (!$publicity): ?>

        <div class="main_box">
            <form class="app_form" action="<?= url("/admin/ads/publicity"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Nome:</span>
                        <input type="text" name="name" placeholder="Nome" required/>
                    </label>

                    <label class="label">
                        <span class="legend">Link:</span>
                        <input type="text" name="link" placeholder="url link"/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">Data da publicação:</span>
                        <input type="text" class="mask-datetime" name="publicity_at" value="<?= date('d/m/Y H:i');?>" required/>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">Página:</span>
                </label>

                <div class="label_g2">
                    <label class="label">
                        <label class="legend" for="home">Home</label>
                        <input id="home" type="checkbox" name="page[]" value="home">
                    </label>
                    <label class="label">
                        <label class="legend" for="artigo">Artigo</label>
                        <input id="artigo" type="checkbox" name="page[]" value="artigo">
                    </label>
                    <label class="label"></label>
                    <label class="label"></label>
                    <label class="label"></label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Local:</span>
                        <select name="local" required>
                            <option value="1">Pop Up</option>
                            <option value="2">Abaixo de Destaque 540 x 110 A</option>
                            <option value="3">Abaixo de Destaque 540 x 110 B</option>
                            <option value="4">Acima de top10 253 x 150 A</option>
                            <option value="5">Acima de top10 253 x 150 B</option>
                            <option value="6">Acima de top10 253 x 150 C</option>
                            <option value="7">Acima de top10 253 x 150 D</option>
                            <option value="8">Meio do site 1000 x 250</option>
                            <option value="9">Lado direito 253 x 150 A</option>
                            <option value="10">Lado direito 253 x 150 B</option>
                            <option value="11">Lado direito 253 x 150 C</option>
                            <option value="12">Lado direito 253 x 150 D</option>
                            <option value="13">Lado direito 253 x 150 E</option>
                        </select>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">Imagem:</span>
                    <input type="file" name="photo"/>
                </label>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Status:</span>
                        <select name="status" required>
                            <option value="post">Publicar</option>
                            <option value="draft">Rascunho</option>
                            <option value="trash ">Lixo</option>
                        </select>
                    </label>
                </div>

                <div class="al-right">
                    <button class="btn btn-green"><i class="far fa-save"></i>Criar Publicidade</button>
                </div>
            </form>
        </div>
    <?php else: ?>
        <div class="main_box">
            <form class="app_form" action="<?= url("/admin/ads/publicity/{$publicity->id}"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Nome:</span>
                        <input type="text" name="name" placeholder="Nome" value="<?= $publicity->name; ?>" required/>
                    </label>

                    <label class="label">
                        <span class="legend">Link:</span>
                        <input type="text" name="link" placeholder="url link" value="<?= $publicity->link; ?>"/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">Data da publicação:</span>
                        <input type="text" class="mask-datetime datetimepicker" name="publicity_at" value="<?= date_fmt_br($publicity->publicity_at) ;?>" required/>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">Página:</span>
                </label>

                <div class="label_g2">
                    <?php
                    $pag = explode(',',$publicity->page);
                    $checked = function ($value) use ($pag) {
                        return ((in_array($value, $pag)) ? "checked" : "");
                    };
                    ?>
                    <label class="label">
                        <label class="legend" for="home">Home</label>
                        <input id="home" type="checkbox" value="home" name="page[]" <?= $checked("home"); ?>>
                    </label>
                    <label class="label">
                        <label class="legend" for="artigo">Artigo</label>
                        <input id="artigo" type="checkbox" value="artigo" name="page[]" <?= $checked("artigo"); ?>>
                    </label>
                    <label class="label"></label>
                    <label class="label"></label>
                    <label class="label"></label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Local:</span>
                        <?php
                        $l = $publicity->local;
                        $select = function ($value) use ($l) {
                            return ($l == $value ? "selected" : "");
                        };
                        ?>
                        <select name="local" required>
                            <option value="1" <?=$select(1);?>>Pop Up</option>
                            <option value="2" <?=$select(2);?>>Abaixo de Destaque 540 x 110 A</option>
                            <option value="3" <?=$select(3);?>>Abaixo de Destaque 540 x 110 B</option>
                            <option value="4" <?=$select(4);?>>Acima de top10 253 x 150 A</option>
                            <option value="5" <?=$select(5);?>>Acima de top10 253 x 150 B</option>
                            <option value="6" <?=$select(6);?>>Acima de top10 253 x 150 C</option>
                            <option value="7" <?=$select(7);?>>Acima de top10 253 x 150 D</option>
                            <option value="8" <?=$select(8);?>>Meio do site 1000 x 250</option>
                            <option value="9" <?=$select(9);?>>Lado direito 253 x 150 A</option>
                            <option value="10" <?=$select(10);?>>Lado direito 253 x 150 B</option>
                            <option value="11" <?=$select(11);?>>Lado direito 253 x 150 C</option>
                            <option value="12" <?=$select(12);?>>Lado direito 253 x 150 D</option>
                            <option value="13" <?=$select(13);?>>Lado direito 253 x 150 E</option>
                        </select>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">Imagem:</span>
                    <input type="file" name="photo"/>
                </label>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Status:</span>
                        <?php
                        $sta = $publicity->status;
                        $select = function ($value) use ($sta) {
                            return ($sta == $value ? "selected" : "");
                        };
                        ?>
                        <select name="status" required>
                            <option value="post" <?=$select('post');?>>Publicar</option>
                            <option value="draft" <?=$select('draft');?>>Rascunho</option>
                            <option value="trash" <?=$select('trash');?>>Lixo</option>
                        </select>
                    </label>
                </div>

                <div class="app_form_footer">
                    <button class="btn btn-blue"><i class="far fa-sync"></i>Atualizar</button>
                    <a href="#" class="remove_link"
                       data-post="<?= url("/admin/ads/publicity/{$publicity->id}"); ?>"
                       data-action="delete"
                       data-confirm="ATENÇÃO: Tem certeza que deseja excluir a publicidade? Essa ação não pode ser feita!"
                       data-publicity_id="<?= $publicity->id; ?>"><i class="far fa-trash-alt"></i>Excluir Publicidade</a>
                </div>
            </form>
        </div>
    <?php endif; ?>
</main>