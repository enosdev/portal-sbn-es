<?php $v->layout("_admin"); ?>
<div class="desc"><i class="fas fa-poll"></i> Enquetes</div>

<div class="searsh_form">
    <a class="btn btn-green" href="<?= url("/".PATH_ADMIN."/faq/channel"); ?>"><i class="fas fa-plus"></i>Nova Pergunta</a>
</div>

<main>
    <div class="box-enquete">
        <div class="app_faqs_home">
                
            <?php if (!$channels): ?>
                <div class="message info"><i class="far fa-info fa-2x"></i>Ainda não existem Enquetes cadastradas.</div>
            <?php else: ?>
                <?php foreach ($channels as $channel): ?>
                    <div class="article">
                        <div class="header">
                            <?php
                                $img = image($channel->cover, 70, 70);
                                echo (!empty($channel->cover))? "<img class='radius' src='{$img}'>": '';
                            ?>
                            <h3><?= $channel->channel; ?></h3>
                            <p>Publicação: <?=date_fmt($channel->created_at);?><br>
                            Expira em: <?=date_fmt($channel->expire_at);?><br>
                            <?=($channel->status == "post" ? "<span style='color:var(--color-green)'><i class='fas fa-share-square'></i>Público</span>" : ($channel->status == "draft" ? "<span style='color:var(--color-yellow)'><i class='fas fa-share-square'></i>Rascunho</span>" : "<span style='color:var(--color-red)'><i class='fas fa-share-square'></i>Lixo</span>")); ?><br>
                            <?=($channel->expire_at < date("Y-m-d H:i:s"))? '<span style="color:var(--color-red)"><i class="far fa-clock"></i>Expirado</span>' : '';?></p>
                            <div>
                                <a href="<?= url("/".PATH_ADMIN."/faq/channel/{$channel->id}"); ?>"class="btn btn-blue"><i class="fas fa-pen"></i> Editar Pergunta</a>
                                <a href="<?= url("/".PATH_ADMIN."/faq/question/{$channel->id}"); ?>" class="btn btn-green"><i class="fas fa-plus"></i> Nova Resposta</a>
                            </div>
                        </div>
                        <div>
                            <?php
                            $channelId = $channel->id;
                            $edit = function ($id) use ($channelId) {
                                $url = url("/".PATH_ADMIN."/faq/question/{$channelId}/{$id}");
                                return "<a href=\"{$url}\" class=\"btn btn-blue fas fa-pen\"></a>";
                            };
                            ?>

                            <?php if (!$channel->questions()->count()): ?>
                                <div class="message info icon-info al-center">
                                    Ainda não existem Respostas
                                </div>
                            <?php else: 
                                $votes = 0;
                            foreach ($channel->questions()->fetch(true) as $question):
                                        $votes += $question->votes;
                                    ?>
                                    <div class="question radius">
                                        <?= $edit($question->id); ?> - <?= $question->question; ?> >>> <strong><?=$question->votes;?> votos</strong>
                                    </div>
                                <?php endforeach; ?>
                                <hr>
                                    Total de votos: <?=$votes;?>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>

        <?= $paginator; ?>
    </div>
</main>