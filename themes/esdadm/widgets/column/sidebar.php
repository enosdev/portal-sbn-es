<div class="main_sidebar">
    <h3><i class="fas fa-laptop"></i> dash/coluna</h3>
    <p class="dash_content_sidebar_desc">Aqui você gerencia todos os artigos da coluna...</p>

    <?php if (!empty($post->cover)): ?>
        <div>
            <h2 style="font-size:var(--font-small);margin:26px 0 13px 0"><i class="fa fa-pencil"></i> Editar coluna #<?=$post->id;?></h2>
            <a class="icon-link btn btn-green" href="<?=url("/artigo/{$post->uri}");?>" target="_blank" title=""><i class="fa fa-link"></i> Ver no
                site</a>
        </div>
        <div id="image-holder">
            <img class="radius" style="width: 100%; margin-top: 30px" src="<?=image($post->cover, 300);?>"/>
        </div>
    <?php else: ?>
        <div id="image-holder"></div>
    <?php endif;?>
</div>