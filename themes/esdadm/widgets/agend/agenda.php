<?php $v->layout("_admin"); ?>
<div class="desc"><i class="far fa-calendar-alt"></i> Agenda de Eventos</div>

<main>
    <?php $v->insert("widgets/agend/sidebar.php"); ?>
    <?php if (!$post): ?>
        <div class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/agend/agenda"); ?>" method="post">
                <!-- ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>

                <label class="label">
                    <span class="legend">*Nome do Evento:</span>
                    <input type="text" name="title" placeholder="Títuo do evento" required/>
                </label>

                <label class="label">
                    <span class="legend">Capa: (1920x1080px)</span>
                    <input type="file" name="cover" placeholder="Uma imagem de capa"/>
                </label>
                <label class="label">
                    <span class="legend">*Local:</span>
                    <input type="text" name="local" placeholder="Local que sediará o evento" required/>
                </label>
                <label class="label">
                    <span class="legend">*Cidade:</span>
                    <input type="text" name="city" placeholder="Cidade do evento" required/>
                </label>
                <label class="label">
                    <span class="legend">*Data do evento:</span>
                    <input class="mask-datetime datetimepicker" type="text" name="event_date" value="<?= date("d/m/Y H:i"); ?>"
                            required/>
                </label>

                <label class="label">
                    <span class="legend">Detalhes:</span>
                    <textarea name="details"></textarea>
                </label>

                <div class="label_g2">
                    <label class="label"></label>
                    <!-- <label class="label">
                        <span class="legend">Estado:</span>
                        <select name="estate" required>
                            <option value="">Selecionar um estado</option>
                            <option value="ba">Bahia</option>
                            <option value="es">Espírito Santo</option>
                        </select>
                    </label> -->
                    <label class="label">
                        <span class="legend">*Data de publicação:</span>
                        <input class="mask-datetime datetimepicker" type="text" name="post_at" value="<?= date("d/m/Y H:i"); ?>"
                               required/>
                    </label>
                </div>

                <div class="label_g2">
                     <label class="label">
                        <span class="legend">*Autor:</span>
                        <select name="author" required>
                            <option value="<?= user()->id; ?>"><?= user()->fullName(); ?></option>
                        </select>
                    </label>
                    <label class="label">
                        <span class="legend">*Status:</span>
                        <select name="status" required>
                            <option value="post">Publicar</option>
                            <option value="draft">Rascunho</option>
                            <option value="trash">Lixo</option>
                        </select>
                    </label>                    
                </div>

                <div class="al-right">
                    <button class="btn btn-green"><i class="far fa-save"></i>Publicar</button>
                </div>
            </form>
        </div>
    <?php else: ?>

        <div class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/agend/agenda/{$post->id}"); ?>" method="post">
                <!-- ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>

                <label class="label">
                    <span class="legend">*Nome do Evento:</span>
                    <input type="text" name="title" placeholder="Títuo do evento" value="<?=$post->title;?>" required/>
                </label>

                <label class="label">
                    <span class="legend">Capa: (1920x1080px)</span>
                    <input type="file" name="cover" placeholder="Uma imagem de capa"/>
                </label>
                <label class="label">
                    <span class="legend">*Local:</span>
                    <input type="text" name="local" placeholder="Local que sediará o evento" value="<?=$post->local;?>" required/>
                </label>
                <label class="label">
                    <span class="legend">*Cidade:</span>
                    <input type="text" name="city" placeholder="Cidade do evento" value="<?=$post->city;?>" required/>
                </label>
                <label class="label">
                    <span class="legend">*Data do evento:</span>
                    <input class="mask-datetime datetimepicker" type="text" name="event_date" value="<?= date_fmt($post->event_date, "d/m/Y H:i"); ?>"
                            required/>
                </label>

                <label class="label">
                    <span class="legend">Detalhes:</span>
                    <textarea name="details"><?=$post->details;?></textarea>
                </label>

                <div class="label_g2">
                    <label class="label"></label>
                    <!-- <label class="label">
                        <span class="legend">Estado:</span>
                        < ?php
                            $estate = $post->estate;
                            $select = function ($value) use ($estate) {
                                return ($estate == $value ? "selected" : "");
                            };
                            ?>
                        <select name="estate" required>
                            <option value="">Selecionar um estado</option>
                            <option < ?= $select("ba"); ?> value="ba">Bahia</option>
                            <option < ?= $select("es"); ?> value="es">Espírito Santo</option>
                        </select>
                    </label> -->
                    <label class="label">
                        <span class="legend">*Data de publicação:</span>
                        <input class="mask-datetime datetimepicker" type="text" name="post_at"
                               value="<?= date_fmt($post->post_at, "d/m/Y H:i"); ?>" required/>
                    </label>
                </div>

                <div class="label_g2">
                <label class="label">
                        <span class="legend">Autor:</span>
                        <select>
                            <option><?= $authors->fullName(); ?></option>
                        </select>
                    </label>
                    <label class="label">
                        <span class="legend">*Status:</span>
                        <select name="status" required>
                            <?php
                            $status = $post->status;
                            $select = function ($value) use ($status) {
                                return ($status == $value ? "selected" : "");
                            };
                            ?>
                            <option <?= $select("post"); ?> value="post">Publicar</option>
                            <option <?= $select("draft"); ?> value="draft">Rascunho</option>
                            <option <?= $select("trash"); ?> value="trash">Lixo</option>
                        </select>
                    </label>
                </div>

                <div class="al-right">
                    <button class="btn btn-blue"><i class="fa fa-sync"></i> Atualizar</button>
                </div>
            </form>
        </div>
    <?php endif; ?>
</main>