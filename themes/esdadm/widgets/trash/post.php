<?php $v->layout("_admin"); ?>
<div class="desc"><i class="far fa-edit"></i> Artigos</div>

<div class="mce_upload" style="z-index: 997">
    <div class="mce_upload_box">
        <form class="app_form" action="<?= url("/".PATH_ADMIN."/blog/post"); ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="upload" value="true"/>
            <label>
                <label class="legend">Selecione uma imagem JPG ou PNG:</label>
                <input accept="image/*" type="file" name="image" required/>
            </label>
            <button class="btn btn-blue icon-upload">Enviar Imagem</button>
        </form>
    </div>
</div>
<div class="mce_upload2" style="z-index: 997">
    <div class="mce_upload_box">
        <form class="app_form" action="<?= url("/".PATH_ADMIN."/blog/post"); ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="upload_file" value="true"/>
            <label>
                <label class="legend">Selecione uma Arquivo:</label>
                <input accept="application/pdf" type="file" name="arch" required/>
            </label>
            <button class="btn btn-blue icon-upload">Enviar Arquivo</button>
        </form>
    </div>
</div>

<main>
    <?php $v->insert("widgets/blog/sidebar.php"); ?>
    <?php if (!$post): ?>
        <div class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/blog/post"); ?>" method="post">
                <!-- ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>

                <label class="label">
                    <span class="legend">*Título:</span>
                    <input type="text" name="title" placeholder="A manchete do seu artigo" required/>
                </label>

                <label class="label">
                    <span class="legend">Subtítulo:</span>
                    <textarea name="subtitle" placeholder="O texto de apoio da manchete"></textarea>
                </label>

                <label class="label">
                    <span class="legend">Capa: (1920x1080px)</span>
                    <input type="file" name="cover" placeholder="Uma imagem de capa"/>
                </label>

                <label class="label">
                    <span class="legend">Vídeo:</span>
                    <input type="text" name="video" placeholder="O LINK de um vídeo do YouTube"/>
                </label>

                <label class="label">
                    <span class="legend">*Conteúdo:</span>
                    <textarea class="mce" name="content"></textarea>
                </label>

                <div class="label_g2">
                     <label class="label">
                        <span class="legend">Vincular Enquete: <small style="color:red">desabilitado</small></span>
                        <select name="poll" disabled>
                            <option>Enquete 1</option>
                        </select>
                    </label>
                    <label class="label" style="position:relative">
                        <span class="legend">Galeria: <span class="copy">#copiar código</span><span class="alertando">Copiado com sucesso</span> <input style="max-width:170px" class="allowCopy" type="text" value=""></span>
                        <select id="gallery" name="gallery">
                            <option value="0">Selecione uma Galeria</option>
                            <?php
                                foreach($gallery as $g):?>
                                    <option value="<?=$g->id;?>"><?=$g->title;?></option>
                            <?php endforeach;?>
                        </select>
                    </label>                    
                </div>

                <div class="label_g2">
                    <label class="label"></label>
                    <label class="label">
                        <span class="legend">*Data de publicação:</span>
                        <input class="mask-datetime datetimepicker" type="text" name="post_at" value="<?= date("d/m/Y H:i"); ?>"
                               required/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Categoria:</span>
                        <select name="category" required>
                            <?php foreach ($categories as $category): ?>
                                <optgroup label="<?= $category->title; ?>">
                                    <option value="<?= $category->id; ?>"><?= $category->title; ?></option>
                                    <?php
                                        $parent = (new Source\Models\Category())->find("parent = :p", "p={$category->id}")->order("title")->fetch(true);
                                        foreach($parent as $p):?>
                                            <option value="<?= $p->id; ?>"><?= $p->title; ?></option>
                                        <?php endforeach;
                                    ?>
                                </optgroup>
                            <?php endforeach; ?>
                        </select>
                    </label>
                    <label class="label">
                        <span class="legend">Tag:</span>
                        <input type="text" name="tag" placeholder="colocar tag"/>
                    </label>                   
                </div>

                <div class="label_g2">
                     <label class="label">
                        <span class="legend">*Autor:</span>
                        <select name="author" required>

                            <?php foreach ($authors as $author):
                                $authorId = user()->id;
                                $select = function ($value) use ($authorId) {
                                    return ($authorId == $value ? "selected" : "");
                                };
                                ?>
                                <option <?= $select($author->id); ?> value="<?= $author->id; ?>"><?= $author->fullName(); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </label>
                    <label class="label">
                        <span class="legend">*Status:</span>
                        <select name="status" required>
                            <option value="post">Publicar</option>
                            <option value="draft">Rascunho</option>
                            <option value="trash">Lixo</option>
                        </select>
                    </label>                    
                </div>

                <div class="al-right">
                    <button class="btn btn-green"><i class="far fa-save"></i>Publicar</button>
                </div>
            </form>
        </div>
    <?php else: ?>
        <div class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/blog/post/{$post->id}"); ?>" method="post">
                <!-- ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>

                <label class="label">
                    <span class="legend">*Título:</span>
                    <input type="text" name="title" value="<?= $post->title; ?>" placeholder="A manchete do seu artigo"
                           required/>
                </label>

                <label class="label">
                    <span class="legend">*Subtítulo:</span>
                    <textarea name="subtitle" placeholder="O texto de apoio da manchete"
                              required><?= $post->subtitle; ?></textarea>
                </label>

                <label class="label">
                    <span class="legend">Capa: (1920x1080px)</span>
                    <input type="file" name="cover" placeholder="Uma imagem de capa"/>
                </label>

                <label class="label">
                    <span class="legend">Vídeo:</span>
                    <input type="text" name="video" value="<?= $post->video; ?>"
                           placeholder="O LINK de um vídeo do YouTube"/>
                </label>

                <label class="label">
                    <span class="legend">*Conteúdo:</span>
                    <textarea class="mce" name="content"><?= $post->content; ?></textarea>
                </label>

                <div class="label_g2">
                     <label class="label">
                        <span class="legend">Vincular Enquete: <small style="color:red">desabilitado</small></span>
                        <select name="poll" disabled>
                            <option>Enquete 1</option>
                        </select>
                    </label>
                    <label class="label" style="position:relative">
                        <span class="legend">Galeria: <span class="copy">#copiar código</span><span class="alertando">Copiado com sucesso</span> <input style="max-width:170px" class="allowCopy" type="text" value="[[<?=$post->gallery;?>]]"></span>
                        <select id="gallery" name="gallery">
                            <option value="0">Selecione uma Galeria</option>
                            <?php
                                $categoryId = $post->gallery;
                                $select = function ($value) use ($categoryId) {
                                    return ($categoryId == $value ? "selected" : "");
                                };
                                foreach($gallery as $g):?>
                                    <option <?= $select($g->id); ?> value="<?=$g->id;?>"><?=$g->title;?></option>
                            <?php endforeach;?>
                        </select>
                    </label>                  
                </div>

                <div class="label_g2">
                    <label class="label"></label>
                    <label class="label">
                        <span class="legend">*Data de publicação:</span>
                        <input class="mask-datetime datetimepicker" type="text" name="post_at"
                               value="<?= date_fmt($post->post_at, "d/m/Y H:i"); ?>" required/>
                    </label>
                </div>

                <div class="label_g2">

                    
                    <label class="label">
                        <span class="legend">*Categoria:</span>
                        <select name="category" required>
                            <?php
                            $categoryId = $post->category;
                            $select = function ($value) use ($categoryId) {
                                return ($categoryId == $value ? "selected" : "");
                            };
                            foreach ($categories as $category): ?>
                                <optgroup label="<?= $category->title; ?>">
                                    <option <?= $select($category->id); ?> value="<?= $category->id; ?>"><?= $category->title; ?></option>
                                    <?php
                                        $parent = (new Source\Models\Category())->find("parent = :p", "p={$category->id}")->order("title")->fetch(true);
                                        foreach($parent as $p):
                                        ?>
                                            <option <?= $select($p->id); ?> value="<?= $p->id; ?>"><?= $p->title; ?></option>
                                        <?php endforeach;
                                    ?>
                                </optgroup>
                            <?php endforeach; ?>
                        </select>
                    </label>

                    <label class="label">
                        <span class="legend">Tag:</span>
                        <input type="text" name="tag" placeholder="colocar tag" value="<?= $post->tag;?>"/>
                    </label>   
                </div>

                <div class="label_g2">
                <label class="label">
                        <span class="legend">*Autor:</span>
                        <select name="author" required>
                            <?php foreach ($authors as $author):
                                $authorId = $post->author;
                                $select = function ($value) use ($authorId) {
                                    return ($authorId == $value ? "selected" : "");
                                };
                                ?>
                                <option <?= $select($author->id); ?>
                                        value="<?= $author->id; ?>"><?= $author->fullName(); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </label>
                    <label class="label">
                        <span class="legend">*Status:</span>
                        <select name="status" required>
                            <?php
                            $status = $post->status;
                            $select = function ($value) use ($status) {
                                return ($status == $value ? "selected" : "");
                            };
                            ?>
                            <option <?= $select("post"); ?> value="post">Publicar</option>
                            <option <?= $select("draft"); ?> value="draft">Rascunho</option>
                            <option <?= $select("trash"); ?> value="trash">Lixo</option>
                        </select>
                    </label>
                </div>

                <div class="al-right">
                    <button class="btn btn-blue"><i class="far fa-sync"></i>Atualizar</button>
                </div>
            </form>
        </div>
    <?php endif; ?>
</main>