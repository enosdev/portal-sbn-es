<div class="main_sidebar">
    <?php
        $ex_c= explode("/",$app);
    ?>
    <h3><i class="fas fa-laptop"></i> dash/artigo<?=(end($ex_c) == 'categories')?'/categoria':'';?></h3>
    <p class="dash_content_sidebar_desc">Aqui você gerencia <?=(end($ex_c) == 'categories')?'todas as categorias':'todos os artigos';?> do site...</p>
    

    <?php if (!empty($post->cover)):
        if($post->category()->id == 10 || $post->category()->parent == 10):
            $link = url("/artigos-entretenimento/{$post->uri}");
        else:
            $link = url("/artigo/{$post->uri}");
        endif;

        $linkEnt = ($post->category()->id == 10 || $post->category()->parent == 10 || $post->category()->id == 36)? 'artigos-entretenimento' : 'artigo';
    ?>
        <div>
            <h2 style="font-size:var(--font-small);margin:26px 0 13px 0"><i class="fa fa-pencil"></i> Editar artigo#<?= $post->id; ?></h2>
            <a class="btn btn-green" href="<?= url("/{$linkEnt}/{$post->uri}"); ?>" target="_blank" title=""><i class="fa fa-link"></i> Ver no
                site</a>
        </div>
        <div id="image-holder">
            <img class="radius" style="width: 100%; margin-top: 30px" src="<?= image($post->cover, 300); ?>"/>
        </div>
    <?php endif; ?>

    <?php if (!empty($category->cover)): ?>
        <div>
            <h2 style="font-size:var(--font-small);margin:26px 0 13px 0"><i class="fa fa-pencil"></i> <?= $category->title; ?></h2>
            <a class="btn btn-green" href="<?= url("/artigo/em/{$category->uri}"); ?>" target="_blank" title=""><i class="fa fa-link"></i> Ver no
                site</a>
        </div>
        <div id="image-holder">
            <img class="radius" style="width: 100%; margin-top: 30px" src="<?= image($category->cover, 300); ?>"/>
        </div>
    <?php endif; ?>

    <?php if (empty($category->cover) || empty($post->cover)):?>
        <div id="image-holder"></div>
    <?php endif; ?>
</div>
