<?php $v->layout("_admin"); ?>
<div class="desc"><i class="fas fa-cogs"></i> Configurações</div>

<main>
    <div class="box-config">
        <div class="header"><i class="fas fa-tools"></i> Permissões</div>
        <?php foreach($permissions as $permit):?>
        <div class="permit">
            <p><?=$permit->title;?> <small>"<?=$permit->uri;?>"</small></p>
            <hr>
            <p>
                <a href="<?= url("/".PATH_ADMIN."/setting/permission/{$permit->id}"); ?>" title="editar"><i class="fas fa-pen"></i></a>
                <!-- <i href="#" title="deletar"><i class="fas fa-trash"></i></a> -->
                <a href="#" class="fas fa-trash"
                       data-post="<?= url("/".PATH_ADMIN."/setting/permission/{$permit->id}"); ?>"
                       data-action="delete"
                       data-confirm="ATENÇÃO: Tem certeza que deseja excluir esse Permissão? Essa ação não pode ser desfeita!"
                       data-permission_id="<?= $permit->id; ?>"></a>
            </p>
        </div>
        <?php endforeach;?>
    </div>
</main>