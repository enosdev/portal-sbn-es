<?php

// $myIp = $_SERVER['REMOTE_ADDR'];
// $ips = [];
// if(in_array($myIp,$ips)){
// 	die('aguarde...');
// }

// //$verifyAgent = $_SERVER['HTTP_USER_AGENT'];
// $verifyAgent = filter_input(INPUT_SERVER, "HTTP_USER_AGENT", FILTER_DEFAULT);

// $motors = ['python-requests','compatible','WOW64','Java'];

// function strpos_arr($haystack, $needle) {
//     if(!is_array($needle)) $needle = array($needle);
//     foreach($needle as $what) {
//         if((strpos($haystack, $what))!==false):
//             die('qual foi robozinho???');
//         endif;
//     }
// }

// strpos_arr($verifyAgent,$motors);

ob_start();

require __DIR__ . "/vendor/autoload.php";

/**
 * BOOTSTRAP
 */

use CoffeeCode\Router\Router;
use Source\Core\Session;

$session = new Session();
$route = new Router(url(), ":");
$route->namespace("Source\App");

/**
 * WEB ROUTES
 */
$route->group(null);
$route->get("/", "Web:home");
$route->get("/sobre", "Web:about");
$route->get("/contato", "Web:contact");

//api
$route->get("/espiritosanto/{w}/{h}", "Web:espiritoSanto");
$route->get("/espiritosantonovo/{w}/{h}/{q}", "Web:espiritoSantoNovo");

//enquete
$route->group("/enquete");
$route->post("/votar", "Web:enquete");
$route->get("/{uri}","Web:enquetePost");

//artigo
$route->group("/artigo");
$route->get("/", "Web:artigo");
$route->get("/p/{page}", "Web:artigo");
$route->get("/{uri}", "Web:artigoPost");
$route->post("/buscar", "Web:artigoSearch");
$route->get("/buscar/{search}/{page}", "Web:artigoSearch");
$route->get("/em/{category}", "Web:artigoCategory");
$route->get("/lista/{category}", "Web:artigoCategoryPrincipal");
$route->get("/em/{category}/{page}", "Web:artigoCategory");
$route->post("/loadmore", "Web:loadMore");

//paginas
$route->group("/pagina");
$route->get("/{uri}","Web:paginas");

/**
 * ENTRETENIMENTO
 */
//entretenimento
$route->group("/entretenimento");
$route->get("/", "Ett:home");

//destaques - aniversários
$route->group("/destaques");
$route->get("/","Ett:destaque");
$route->get("/{uri}","Ett:destaquePost");
$route->get("/p/{page}", "Ett:destaque");

//lista noticias entretenimento
$route->group("/artigos-entretenimento");
$route->get("/","Ett:artigo");
$route->get("/{uri}","Ett:artigoPost");
$route->get("/p/{page}", "Ett:artigo");

//lista vídeos entretenimento
$route->group("/video-entretenimento");
$route->get("/","Ett:videos");

//gallery - coberturas
$route->group("/coberturas");
$route->get("/", "Ett:galeria");
$route->get("/p/{page}", "Ett:galeria");
$route->post("/buscar", "Ett:galeriaSearch");
$route->get("/buscar/{search}/{page}", "Ett:galeriaSearch");

//gallery - fotos
$route->group("/fotos");
$route->get("/{uri}", "Ett:galeriaPost");
$route->get("/{uri}/p/{page}", "Ett:galeriaPost");

//eventos
$route->group("/evento");
$route->get("/{uri}", "Ett:eventoPost");

//agenda
$route->group("/agenda");
$route->get("/", "Ett:agenda");
$route->get("/p/{page}", "Ett:agenda");

//cobertura
$route->group(null);
$route->get("/divulgue-seu-evento", "Ett:divulge");
$route->get("/solicitar-cobertura", "Ett:requestEvent");

/**
 * ADMIN ROUTES
 */
$route->namespace("Source\App\Admin");
$route->group("/".PATH_ADMIN);

//login
$route->get("/", "Login:root");
$route->get("/login", "Login:login");
$route->post("/login", "Login:login");

//settings
$route->get("/setting/home", "Conf:home");
$route->get("/setting/permission","Conf:permit");
$route->post("/setting/permission","Conf:permit");
$route->get("/setting/permission/{permission_id}","Conf:permit");
$route->post("/setting/permission/{permission_id}","Conf:permit");

//dash
$route->get("/dash", "Dash:dash");
$route->get("/dash/home", "Dash:home");
$route->post("/dash/home", "Dash:home");
$route->get("/logoff", "Dash:logoff");

//trash
$route->get("/trash/home", "Trash:home");
$route->post("/trash/post/{post_id}", "Trash:post");

//blog
$route->get("/blog/home", "Blog:home");
$route->post("/blog/home", "Blog:home");
$route->get("/blog/home/{search}/{page}", "Blog:home");
$route->get("/blog/post", "Blog:post");
$route->post("/blog/post", "Blog:post");
$route->get("/blog/post/{post_id}", "Blog:post");
$route->post("/blog/post/{post_id}", "Blog:post");
$route->get("/blog/categories", "Blog:categories");
$route->get("/blog/categories/{page}", "Blog:categories");
$route->get("/blog/category", "Blog:category");
$route->post("/blog/category", "Blog:category");
$route->get("/blog/category/{category_id}", "Blog:category");
$route->post("/blog/category/{category_id}", "Blog:category");
$route->post("/blog/morenews", "Blog:loadMoreNews");
$route->post("/blog/morenewscategory", "Blog:loadMoreNewsCategory");

//share
$route->get("/share/home", "Share:home");
$route->post("/share/home", "Share:home");
$route->get("/share/home/{search}/{page}", "Share:home");

//Column
$route->get("/column/home", "Column:home");
$route->post("/column/home", "Column:home");
$route->get("/column/home/{search}/{page}", "Column:home");
$route->get("/column/post", "Column:post");
$route->post("/column/post", "Column:post");
$route->get("/column/post/{post_id}", "Column:post");
$route->post("/column/post/{post_id}", "Column:post");

//Pages
$route->get("/pages/home", "Pages:home");
$route->post("/pages/home", "Pages:home");
$route->get("/pages/home/{search}/{page}", "Pages:home");
$route->get("/pages/post", "Pages:post");
$route->post("/pages/post", "Pages:post");
$route->get("/pages/post/{post_id}", "Pages:post");
$route->post("/pages/post/{post_id}", "Pages:post");

//Agenda
$route->get("/agend/home", "Agend:home");
$route->post("/agend/home", "Agend:home");
$route->get("/agend/home/{search}/{page}", "Agend:home");
$route->get("/agend/agenda", "Agend:agenda");
$route->post("/agend/agenda", "Agend:agenda");
$route->get("/agend/agenda/{agenda_id}", "Agend:agenda");
$route->post("/agend/agenda/{agenda_id}", "Agend:agenda");

//users
$route->get("/users/home", "Users:home");
$route->post("/users/home", "Users:home");
$route->get("/users/home/{search}/{page}", "Users:home");
$route->get("/users/user", "Users:user");
$route->post("/users/user", "Users:user");
$route->get("/users/user/{user_id}", "Users:user");
$route->post("/users/user/{user_id}", "Users:user");

//ads
$route->get("/ads/home", "Ads:home");
$route->post("/ads/home", "Ads:home");
$route->get("/ads/home/{search}/{page}", "Ads:home");
$route->get("/ads/publicity", "Ads:publicity");
$route->post("/ads/publicity", "Ads:publicity");
$route->get("/ads/publicity/{publicity_id}", "Ads:publicity");
$route->post("/ads/publicity/{publicity_id}", "Ads:publicity");

//timer stopwatch
$route->get("/timer/home", "Timer:home");
$route->post("/timer/home", "Timer:home");
$route->get("/timer/home/{search}/{page}", "Timer:home");
$route->get("/timer/stopwatch", "Timer:stopwatch");
$route->post("/timer/stopwatch", "Timer:stopwatch");
$route->get("/timer/stopwatch/{stopwatch_id}", "Timer:stopwatch");
$route->post("/timer/stopwatch/{stopwatch_id}", "Timer:stopwatch");

//gallery
$route->get("/gal/home", "Gal:home");
$route->post("/gal/home", "Gal:home");
$route->get("/gal/home/{search}/{page}", "Gal:home");
$route->get("/gal/gallery", "Gal:gallery");
$route->post("/gal/gallery", "Gal:gallery");
$route->get("/gal/gallery/{gallery_id}", "Gal:gallery");
$route->post("/gal/gallery/{gallery_id}", "Gal:gallery");

// $route->get("/gal/photos", "Gal:photos");
$route->post("/gal/photos", "Gal:photos");
$route->get("/gal/photos/{gallery_id}", "Gal:photos");
// $route->post("/gal/photos/{gallery_id}", "Gal:photos");

//notification center
$route->post("/notifications/count", "Notifications:count");
$route->post("/notifications/list", "Notifications:list");

//faqs
$route->get("/faq/home", "Faq:home");
$route->get("/faq/home/{page}", "Faq:home");
$route->get("/faq/channel", "Faq:channel");
$route->post("/faq/channel", "Faq:channel");
$route->get("/faq/channel/{channel_id}", "Faq:channel");
$route->post("/faq/channel/{channel_id}", "Faq:channel");
$route->get("/faq/question/{channel_id}", "Faq:question");
$route->post("/faq/question/{channel_id}", "Faq:question");
$route->get("/faq/question/{channel_id}/{question_id}", "Faq:question");
$route->post("/faq/question/{channel_id}/{question_id}", "Faq:question");

//destaque
$route->get("/dest/home", "Dest:home");
$route->post("/dest/home", "Dest:home");
$route->get("/dest/home/{search}/{page}", "Dest:home");
$route->get("/dest/highlight", "Dest:highlight");
$route->post("/dest/highlight", "Dest:highlight");
$route->get("/dest/highlight/{post_id}", "Dest:highlight");
$route->post("/dest/highlight/{post_id}", "Dest:highlight");


//END ADMIN
$route->namespace("Source\App");

/**
 * ERROR ROUTES
 */
$route->group("/ops");
$route->get("/{errcode}", "Web:error");

/**
 * ROUTE
 */
$route->dispatch();

/**
 * ERROR REDIRECT
 */
if ($route->error()) {
    $route->redirect("/ops/{$route->error()}");
}

ob_end_flush();